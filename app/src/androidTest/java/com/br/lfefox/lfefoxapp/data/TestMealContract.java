package com.br.lfefox.lfefoxapp.data;

import android.net.Uri;
import android.test.AndroidTestCase;

/**
 * Tests related to Meal contract URIs
 */
public class TestMealContract extends AndroidTestCase {

    private static final long PHONE_ID = 11999999999l;
    private static final String URI_PHONE = "content://com.br.lfefox.lfefoxapp/phone/11999999999";
    private static final String URI_MEAL_WIH_PHONE_NUMBER = "content://com.br.lfefox.lfefoxapp/meal/11999999999";

    public void testBuildPhoneUri(){

        Uri locationUri = MealContract.PhoneEntry.buildPhoneUri(PHONE_ID);

        assertNotNull("Error: Null Uri returned.  You must fill-in buildPhoneURI in", locationUri);

        assertTrue("Error: phone id returned in the uri is not equal",
                String.valueOf(PHONE_ID).equalsIgnoreCase(locationUri.getLastPathSegment()));

        assertEquals("Error: Phone Uri doesn't match our expected result", locationUri.toString(), URI_PHONE);


    }


    public void testBuildMealWithPhoneNumberUri(){

        Uri locationUri = MealContract.MealEntry.buildMealWithPhoneNumber(PHONE_ID);

        assertNotNull("Error: Null Uri returned.  You must fill-in buildMealWithPhoneNumber in", locationUri);

        assertTrue("Error: phone id returned in the uri is not equal",
                String.valueOf(PHONE_ID).equalsIgnoreCase(locationUri.getLastPathSegment()));

        assertEquals("Error: Meal Uri doesn't match our expected result", locationUri.toString(), URI_MEAL_WIH_PHONE_NUMBER);

    }
}
