package com.br.lfefox.lfefoxapp.data;

import android.app.Application;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.ApplicationTestCase;

import java.util.HashSet;

/**
 * Test for MealDBHelper and database operations
 */
public class TestDb extends ApplicationTestCase<Application> {

    private static final String PHONE_ID = "11999999999";
    private static final long DATE_INSERT = 1419033600L;  // December 20th, 2014
    private static final String NAME = "Thor";
    private static final String USER_NAME = "thor";
    private static final long BIRTHDAY = 1419033600L;
    private static final String ACESS_KEY = "PASSWORD";


    public TestDb(){
        super(Application.class);
    }

    void deleteTheDatabase() {
        mContext.deleteDatabase(MealDbHelper.DATABASE_NAME);
    }

    public void setUp() {
        deleteTheDatabase();
    }

    /**
     * Test to insert a phone
     *
     */
    public void testInsertPhone(){
        insertPhone();
    }

    /**
     * Test to insert a meal
     *
     */
    public void testInsertMeal(){
        insertMeal();
    }

    /**
     * Test to insert a User
     *
     */
    public void testInsertUser(){insertUser();}

    /**
     * Test to create meal database
     * @throws Throwable
     */
    public void testCreateDb() throws Throwable {

        final HashSet<String> tableNameHashSet = new HashSet<String>();
        tableNameHashSet.add(MealContract.PhoneEntry.TABLE_NAME);
        tableNameHashSet.add(MealContract.MealEntry.TABLE_NAME);
        tableNameHashSet.add(MealContract.UserEntry.TABLE_NAME);

        mContext.deleteDatabase(MealDbHelper.DATABASE_NAME);
        SQLiteDatabase db = new MealDbHelper(
                this.mContext).getWritableDatabase();
        assertEquals(true, db.isOpen());

        // CHECK if tables were created
        Cursor c = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);

        assertTrue("Error: This means that the database has not been created correctly", c.moveToFirst());

        do {
            tableNameHashSet.remove(c.getString(0));
        } while( c.moveToNext() );

        //tableNameHashSet must be empty
        assertTrue("Error: Your database was created without both the Phone entry and Meal entry tables", tableNameHashSet.isEmpty());

        c.close();
        db.close();
    }



    public long insertPhone(){
        MealDbHelper dbHelper = new MealDbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues testValues = new ContentValues();
        testValues.put(MealContract.PhoneEntry._ID, PHONE_ID);
        testValues.put(MealContract.PhoneEntry.COLUMN_REGISTER_DATE, DATE_INSERT);

        // Third Step: Insert ContentValues into database and get a row ID back
        long locationRowId;
        locationRowId = db.insert(MealContract.PhoneEntry.TABLE_NAME, null, testValues);


        assertTrue(locationRowId != -1);

        //Just a test to recover the inserted object
        Cursor cursor = db.query(MealContract.PhoneEntry.TABLE_NAME, null, MealContract.PhoneEntry._ID + " = ?", new String[]{PHONE_ID}, null, null, null);

        cursor.moveToFirst();
        assertTrue("Error: No Records returned from location query", cursor.getString(0).equalsIgnoreCase(PHONE_ID));


        locationRowId = db.insert(MealContract.PhoneEntry.TABLE_NAME, null, testValues);

        assertTrue("Phone number is primary key, trying to insert the same key must return -1", locationRowId == -1l);

        cursor.close();
        db.close();
        return locationRowId;
    }

    public long insertMeal(){
        insertPhone();
        MealDbHelper dbHelper = new MealDbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues testValues = new ContentValues();
        testValues.put(MealContract.MealEntry.COLUMN_PHONE_NUMBER, PHONE_ID);
        testValues.put(MealContract.MealEntry.COLUMN_VALID, 1);
        testValues.put(MealContract.MealEntry.COLUMN_CHECKIN_DATE, DATE_INSERT);

        long locationRowId;
        locationRowId = db.insert(MealContract.MealEntry.TABLE_NAME, null, testValues);

        assertTrue(locationRowId != -1);

        //Just a test to recover the inserted object
        Cursor cursor = db.query( MealContract.MealEntry.TABLE_NAME, null, MealContract.MealEntry._ID + " = ?",   new String[] {String.valueOf(locationRowId)}, null, null, null);

        cursor.moveToFirst();
        assertTrue("Error: No Records returned from location query", cursor.getString(0).equalsIgnoreCase(String.valueOf(locationRowId)));

        cursor.close();
        db.close();
        return locationRowId;
    }

    public long insertUser(){

        MealDbHelper dbHelper = new MealDbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        ContentValues testValues = new ContentValues();
        testValues.put(MealContract.UserEntry.NAME, NAME);
        testValues.put(MealContract.UserEntry.USER_NAME, USER_NAME);
        testValues.put(MealContract.UserEntry.BIRTHDAY, BIRTHDAY);
        testValues.put(MealContract.UserEntry.ACESS_KEY, ACESS_KEY);

        long userRowId;
        userRowId = db.insert(MealContract.UserEntry.TABLE_NAME, null, testValues);

        assertTrue(userRowId != -1);

        //Just a test to recover the inserted object
        Cursor cursor = db.query( MealContract.UserEntry.TABLE_NAME, null, MealContract.UserEntry._ID + " = ?",   new String[] {String.valueOf(userRowId)}, null, null, null);

        cursor.moveToFirst();
        assertTrue("Error: No Records returned from location query", cursor.getString(0).equalsIgnoreCase(String.valueOf(userRowId)));

        cursor.close();
        db.close();

        return userRowId;
    }

}
