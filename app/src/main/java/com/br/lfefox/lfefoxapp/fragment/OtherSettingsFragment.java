package com.br.lfefox.lfefoxapp.fragment;


import android.app.Dialog;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.br.lfefox.lfefoxapp.R;
import com.br.lfefox.lfefoxapp.constants.MealConstants;


public class OtherSettingsFragment extends Fragment {


    public OtherSettingsFragment() {}

    private View inflatedView;
    private EditText checkinNumberForPrize;
    private EditText nameOfService;
    private TextView checkinNumberInfo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        inflatedView = inflater.inflate(R.layout.fragment_other_settings, container, false);
        configureBindsAndInfo();
        return inflatedView;
    }

    private void configureBindsAndInfo(){
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
        int defaultCounter =  prefs.getInt(MealConstants.KEY_PREF_CHECKIN_NUMBER, MealConstants.DEFAULT_CHECKIN_NUMBER);
        String serviceOffered = prefs.getString(MealConstants.KEY_PREF_SERVICE_NAME, getResources().getString(R.string.label_default_service_name));

        checkinNumberForPrize = (EditText) inflatedView.findViewById(R.id.checkinNumberForPrize);
        checkinNumberForPrize.setText(String.valueOf(defaultCounter));

        checkinNumberInfo = (TextView)inflatedView.findViewById(R.id.labelCheckinNumberInfo);
        checkinNumberInfo.setText(getResources().getString(R.string.label_checkin_number_info, serviceOffered));

        nameOfService = (EditText) inflatedView.findViewById(R.id.nameOfService);
        nameOfService.setText(serviceOffered);

        final Button btnSave = (Button) inflatedView.findViewById(R.id.btnSaveSettings);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SharedPreferences.Editor  editor = prefs.edit();
                editor.putInt(MealConstants.KEY_PREF_CHECKIN_NUMBER, Integer.valueOf(checkinNumberForPrize.getText().toString()));
                editor.putString(MealConstants.KEY_PREF_SERVICE_NAME, nameOfService.getText().toString());
                editor.commit();

                checkinNumberInfo.setText(getResources().getString(R.string.label_checkin_number_info, nameOfService.getText().toString()));

                Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.other_settings_confirmation);

                TextView textView = (TextView)dialog.findViewById(R.id.labelConfirmationMsg);
                textView.setText(R.string.msg_prefs_saved);
                ImageView img = (ImageView) dialog.findViewById(R.id.imgConfirmationMsg);
                img.setImageResource(R.drawable.checkedmark);
                dialog.show();

            }
        });


    }

}
