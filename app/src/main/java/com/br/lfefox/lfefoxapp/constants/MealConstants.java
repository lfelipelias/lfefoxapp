package com.br.lfefox.lfefoxapp.constants;

/**
 * Class with constants for the app
 */
public class MealConstants {

    //Information used by MealService and MealBroadcastReceiver
    public static final String BROADCAST_ACTION = "com.br.lfefox.lfefoxapp.meal.BROADCAST";
    public static final String EXTENDED_DATA_STATUS = "com.br.lfefox.lfefoxapp.meal.STATUS";
    public static final String EXTENDED_DATA_RETURN_MESSAGE = "com.br.lfefox.lfefoxapp.checkin.RETURN_MESSAGE";
    public static final String BROADCAST_USER_ACTION = "com.br.lfefox.lfefoxapp.user.BROADCAST";
    public static final String EXTENDED_DATA_STATUS_USER = "com.br.lfefox.lfefoxapp.user.STATUS";

    public static int STATUS_CHECKIN_OK = 1;
    public static int STATUS_CHECKIN_NOK = 0;
    public static int STATUS_CHECKIN_PRIZE = 2;
    public static int STATUS_CHECKIN_VALID = 1;
    public static int STATUS_CHECKIN_INVALID = 0;

    public static int STATUS_REGISTER_OK = 1;
    public static int STATUS_REGISTER_NOK = 0;

    public static int STATUS_LOGIN_NOK = 0;
    public static int STATUS_LOGIN_OK = 1;

    public static int STATUS_REGISTER_CREATED = 1;

    public static int STATUS_RESET_PASS_OK = 1;
    public static int STATUS_RESET_PASS_NOK = 0;
    public static final String KEY_RESET_PREVIOUSLY = "key_reset_prev";
    public static int FLAG_RESET_PREVIOUSLY = 1;

    public static final int CHECK_ADMIN_DOESNT_EXIST = 0;
    public static final int CHECK_ADMIN_EXIST = 1;

    //MealContract.MealEntry.COLUMN_VALID values
    public static final int CHECKIN_VALID = 1;
    public static final int CHECKIN_INVALID = 0;

    public static final int MAX_SIZE_PHONE_NUMBER = 11;
    public static final int MIN_SIZE_PHONE_NUMBER = 10;

    public static final int DEFAULT_CHECKIN_NUMBER = 10;
    public static final String KEY_PREF_CHECKIN_NUMBER = "checkinforprize";
    public static final String KEY_PREF_SERVICE_NAME = "key_service_name";

    public static final String KEY_NAME_EXTRA = "key_name";
    public static final String KEY_USER_NAME_EXTRA = "key_user_name";
    public static final String KEY_BIRTHDAY_EXTRA = "key_birthday";
    public static final String KEY_ACCESS_KEY_EXTRA = "key_access_key";
    public static final String KEY_DT_INIT = "key_dt_init";
    public static final String KEY_DT_END = "key_dt_end";
    public static final String KEY_PHONE_NUMBER = "key_phone_number";
    public static final String KEY_CURRENT_PAGE_ADMIN = "key_current_page_admin";
    public static final String KEY_COUNT_ALL_CHECKINS = "key_count_all_checkin";
    public static final String KEY_COUNT_VALID_CHECKINS = "key_count_valid_checkin";
    public static final String KEY_LAST_DATE_CHECKIN = "key_last_date_checkin";
    public static final String KEY_PHONE_NOT_FOUND = "key_phone_not_found";
    public static final String USER_SERVICE_ACTION = "user_service_action";
    public static final int USER_SERVICE_ACTION_REGISTER = 1;
    public static final int USER_SERVICE_ACTION_RESET_PASS = 2;
    public static final int USER_SERVICE_ACTION_CHECK_ADMIN = 3;
    public static final int USER_SERVICE_ACTION_LOGIN = 4;
    public static final int USER_SERVICE_ACTION_RESET_CONFIRMATION = 5;

    public static final String KEY_ADMIN_EXIST = "key_user_exist";
    public static final String KEY_LIST_CHECKINS = "key_list_checkins";
    public static final String KEY_REGISTER_CREATED = "key_register_created";

    public static final String MEAL_SERVICE_ACTION = "meal_service_action";
    public static final int MEAL_SERVICE_ACTION_CHECKIN = 1;
    public static final int MEAL_SERVICE_ACTION_FIND_MEALS = 2;
    public static final int MEAL_SERVICE_ACTION_FIND_CLIENT_INFO = 3;



}
