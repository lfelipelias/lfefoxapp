package com.br.lfefox.lfefoxapp;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;

import com.br.lfefox.lfefoxapp.constants.MealConstants;
import com.br.lfefox.lfefoxapp.fragment.WelcomeFragment;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    FrameLayout frameMainContent;
    FragmentManager fm;

    @Inject
    SharedPreferences prefs;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((MyApp)getApplication()).getComponent().inject(this);

        setupDefaults();

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);



        frameMainContent = (FrameLayout)findViewById(R.id.frame_main_content);
        fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_main_content, new WelcomeFragment());
        ft.commit();



    }

    /**
     * This method is used to setup the default number of checkins required for the user to win the free meal
     */
    private void setupDefaults(){

        if(!prefs.contains(MealConstants.KEY_PREF_CHECKIN_NUMBER)){
            SharedPreferences.Editor  editor = prefs.edit();
            editor.putInt(MealConstants.KEY_PREF_CHECKIN_NUMBER, MealConstants.DEFAULT_CHECKIN_NUMBER);
            editor.commit();
        }

        if(!prefs.contains(MealConstants.KEY_PREF_SERVICE_NAME)){
            SharedPreferences.Editor  editor = prefs.edit();
            editor.putString(MealConstants.KEY_PREF_SERVICE_NAME, getResources().getString(R.string.label_default_service_name));
            editor.commit();
        }

    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

}
