package com.br.lfefox.lfefoxapp.service;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.content.LocalBroadcastManager;

import com.br.lfefox.lfefoxapp.constants.MealConstants;
import com.br.lfefox.lfefoxapp.data.MealContract;
import com.br.lfefox.lfefoxapp.data.MealProviderUtils;
import com.br.lfefox.lfefoxapp.util.Base64;

/**
 * This Service is responsible to perform background tasks for MealApp
 */
public class UserService extends IntentService {


    public UserService() {
        super("MealService");
    }
    public UserService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        int action = (int)  intent.getExtras().get(MealConstants.USER_SERVICE_ACTION);

        switch (action) {
            case MealConstants.USER_SERVICE_ACTION_REGISTER: {
                registerUser(intent);
                break;
            } case MealConstants.USER_SERVICE_ACTION_CHECK_ADMIN: {
                checkUserAdmin();
                break;
            } case MealConstants.USER_SERVICE_ACTION_LOGIN: {
                loginUser(intent);
                break;
            } case MealConstants.USER_SERVICE_ACTION_RESET_PASS: {
                checkResetPassword(intent);
                break;
            } case MealConstants.USER_SERVICE_ACTION_RESET_CONFIRMATION: {
                resetPassword(intent);
                break;
            }
        }

    }

    /**
     * Method which performs validation and user registration
     * @param intent
     */
    private void registerUser(Intent intent){

        //Only one user will be registered to control the app
        if(userDoesntExist()){

            String accessKey = (String) intent.getExtras().get(MealConstants.KEY_ACCESS_KEY_EXTRA);
            long birthday = (long) intent.getExtras().get(MealConstants.KEY_BIRTHDAY_EXTRA);

            String key = accessKey+"|"+String.valueOf(birthday);

            String encryptedKey = Base64.encodeBytes(key.getBytes());

            ContentValues insertUser = new ContentValues();
            insertUser.put(MealContract.UserEntry.NAME, (String) intent.getExtras().get(MealConstants.KEY_NAME_EXTRA));
            insertUser.put(MealContract.UserEntry.USER_NAME, (String) intent.getExtras().get(MealConstants.KEY_USER_NAME_EXTRA));
            insertUser.put(MealContract.UserEntry.BIRTHDAY, birthday);
            insertUser.put(MealContract.UserEntry.ACESS_KEY, encryptedKey);

            Uri uri = getApplicationContext().getContentResolver().insert(MealContract.UserEntry.CONTENT_URI, insertUser);

            //Setting the response to the UserBroadCastReceiver
            Intent localIntent = new Intent(MealConstants.BROADCAST_USER_ACTION);
                localIntent.putExtra(MealConstants.USER_SERVICE_ACTION, MealConstants.USER_SERVICE_ACTION_REGISTER);
                localIntent.putExtra(MealConstants.EXTENDED_DATA_STATUS_USER, MealConstants.STATUS_REGISTER_OK);

            LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);

        } else {
            Intent localIntent = new Intent(MealConstants.BROADCAST_USER_ACTION);
            localIntent.putExtra(MealConstants.USER_SERVICE_ACTION, MealConstants.USER_SERVICE_ACTION_REGISTER);
            localIntent.putExtra(MealConstants.EXTENDED_DATA_STATUS_USER, MealConstants.STATUS_REGISTER_NOK);
            LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
        }

    }

    /**
     * This method is used to check if there is a user in the user table
     * @return boolean
     */
    private boolean userDoesntExist(){

        Cursor cursorUser = getApplicationContext().getContentResolver().query(MealContract.UserEntry.CONTENT_URI, null, null, null, null);
        cursorUser.close();
        return cursorUser.getCount() == 0;
    }

    private void checkUserAdmin(){
        boolean doesntExist = userDoesntExist();
        Intent localIntent = new Intent(MealConstants.BROADCAST_USER_ACTION);

        localIntent.putExtra(MealConstants.USER_SERVICE_ACTION, MealConstants.USER_SERVICE_ACTION_CHECK_ADMIN);
        localIntent.putExtra(MealConstants.EXTENDED_DATA_STATUS_USER,
                doesntExist ? MealConstants.CHECK_ADMIN_DOESNT_EXIST : MealConstants.CHECK_ADMIN_EXIST);
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);


    }

    private void loginUser(Intent intent){
        String username = (String) intent.getExtras().get(MealConstants.KEY_USER_NAME_EXTRA);
        String password = (String) intent.getExtras().get(MealConstants.KEY_ACCESS_KEY_EXTRA);
        final String [] selectionUser = new String[]{username};

        Intent localIntent = new Intent(MealConstants.BROADCAST_USER_ACTION);
        localIntent.putExtra(MealConstants.USER_SERVICE_ACTION, MealConstants.USER_SERVICE_ACTION_LOGIN);

        Cursor cursorUser = getApplicationContext().getContentResolver().query(MealContract.UserEntry.CONTENT_URI, null, MealProviderUtils.whereUserByUsername, selectionUser, null);

        if(cursorUser.getCount() == 0 ){
            localIntent.putExtra(MealConstants.EXTENDED_DATA_STATUS_USER, MealConstants.STATUS_LOGIN_NOK);
        } else if(cursorUser.getCount() == 1 ){

            cursorUser.moveToFirst();

            String key =  password+"|"+cursorUser.getString(3);
            key = Base64.encodeBytes(key.getBytes());

            //check for user and password
            if(username.equalsIgnoreCase(cursorUser.getString(2))
                    && key.equalsIgnoreCase(cursorUser.getString(4))){
                localIntent.putExtra(MealConstants.EXTENDED_DATA_STATUS_USER, MealConstants.STATUS_LOGIN_OK);
            } else {
                //wrong user or password
                localIntent.putExtra(MealConstants.EXTENDED_DATA_STATUS_USER, MealConstants.STATUS_LOGIN_NOK);
            }

        }

        cursorUser.close();
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);

    }

    private void checkResetPassword(Intent intent) {
        String userName = (String) intent.getExtras().get(MealConstants.KEY_USER_NAME_EXTRA);
        long birthday = (long) intent.getExtras().get(MealConstants.KEY_BIRTHDAY_EXTRA);

        final String [] selectionUser = new String[]{userName, String.valueOf(birthday)};

        Cursor cursorUser = getApplicationContext().getContentResolver().query(MealContract.UserEntry.CONTENT_URI, null, MealProviderUtils.whereUserByUsernameBirthDay, selectionUser, null);
        cursorUser.close();

        Intent localIntent = new Intent(MealConstants.BROADCAST_USER_ACTION);
        localIntent.putExtra(MealConstants.USER_SERVICE_ACTION, MealConstants.USER_SERVICE_ACTION_RESET_PASS);

        if(cursorUser.getCount() == 0){
            //user informed wrong data
            localIntent.putExtra(MealConstants.EXTENDED_DATA_STATUS_USER, MealConstants.STATUS_RESET_PASS_NOK);
        } else {
            localIntent.putExtra(MealConstants.EXTENDED_DATA_STATUS_USER, MealConstants.STATUS_RESET_PASS_OK);
        }

        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }

    private void resetPassword(Intent intent){

        String accessKey = (String) intent.getExtras().get(MealConstants.KEY_ACCESS_KEY_EXTRA);
        long birthday = (long) intent.getExtras().get(MealConstants.KEY_BIRTHDAY_EXTRA);

        String key = accessKey+"|"+String.valueOf(birthday);

        String encryptedKey = Base64.encodeBytes(key.getBytes());

        ContentValues updateMealValues = new ContentValues();
        updateMealValues.put(MealContract.UserEntry.ACESS_KEY, encryptedKey);


        String where = MealContract.UserEntry.TABLE_NAME+ "."+ MealContract.UserEntry.USER_NAME+ " = ? AND " + MealContract.UserEntry.TABLE_NAME+ "."+MealContract.UserEntry.BIRTHDAY + " = ?";
        String[] selection = new String[]{(String) intent.getExtras().get(MealConstants.KEY_USER_NAME_EXTRA), String.valueOf(birthday)};
        int countUpdates = getApplicationContext().getContentResolver().update(MealContract.UserEntry.CONTENT_URI, updateMealValues, where,  selection);

        Intent localIntent = new Intent(MealConstants.BROADCAST_USER_ACTION);
        localIntent.putExtra(MealConstants.USER_SERVICE_ACTION, MealConstants.USER_SERVICE_ACTION_RESET_CONFIRMATION);

        if(countUpdates > 0){
            localIntent.putExtra(MealConstants.EXTENDED_DATA_STATUS_USER, MealConstants.STATUS_RESET_PASS_OK);
        } else {
            localIntent.putExtra(MealConstants.EXTENDED_DATA_STATUS_USER, MealConstants.STATUS_RESET_PASS_NOK);
        }

        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);

    }

}
