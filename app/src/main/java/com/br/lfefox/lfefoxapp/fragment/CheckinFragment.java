package com.br.lfefox.lfefoxapp.fragment;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.br.lfefox.lfefoxapp.R;
import com.br.lfefox.lfefoxapp.constants.MealConstants;
import com.br.lfefox.lfefoxapp.data.MealContract;
import com.br.lfefox.lfefoxapp.service.MealService;

import java.util.ArrayList;
import java.util.List;

public class CheckinFragment extends Fragment {

    private View inflatedView;
    private EditText editText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        inflatedView = inflater.inflate(R.layout.fragment_checkin, container, false);
        Toolbar toolbar = (Toolbar) inflatedView.findViewById(R.id.toolbar);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);

        return inflatedView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onResume() {
        super.onResume();

        editText = (EditText) inflatedView.findViewById(R.id.phone_number_input);
        editText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MealConstants.MAX_SIZE_PHONE_NUMBER)});
        configureNumericKeyboard();
        configureOkButtonCheckin();

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    private void configureOkButtonCheckin(){
        Button btn = (Button)inflatedView.findViewById(R.id.checkin_fragment_ok);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editText.getText().toString().length() >= MealConstants.MIN_SIZE_PHONE_NUMBER){

                    //Calling MealService
                    Intent intent = new Intent(getActivity().getApplicationContext(), MealService.class);
                    intent.setData(MealContract.MealEntry.buildMealUri(Long.valueOf(editText.getText().toString())));
                    intent.putExtra(MealConstants.MEAL_SERVICE_ACTION, MealConstants.MEAL_SERVICE_ACTION_CHECKIN);
                    getActivity().getApplicationContext().startService(intent);


                } else {
                     editText.setError(getResources().getString(R.string.label_phone_size_invalid, MealConstants.MIN_SIZE_PHONE_NUMBER));
                }
            }
        });

    }
    private void configureNumericKeyboard(){
        List<TextView> keys = new ArrayList<>();

        keys.add((TextView)inflatedView.findViewById(R.id.anti_theft_t9_key_0));
        keys.add((TextView)inflatedView.findViewById(R.id.anti_theft_t9_key_1));
        keys.add((TextView)inflatedView.findViewById(R.id.anti_theft_t9_key_2));
        keys.add((TextView)inflatedView.findViewById(R.id.anti_theft_t9_key_3));
        keys.add((TextView)inflatedView.findViewById(R.id.anti_theft_t9_key_4));
        keys.add((TextView)inflatedView.findViewById(R.id.anti_theft_t9_key_5));
        keys.add((TextView)inflatedView.findViewById(R.id.anti_theft_t9_key_5));
        keys.add((TextView)inflatedView.findViewById(R.id.anti_theft_t9_key_6));
        keys.add((TextView)inflatedView.findViewById(R.id.anti_theft_t9_key_7));
        keys.add((TextView)inflatedView.findViewById(R.id.anti_theft_t9_key_8));
        keys.add((TextView)inflatedView.findViewById(R.id.anti_theft_t9_key_9));
        keys.add((TextView)inflatedView.findViewById(R.id.anti_theft_t9_key_clear));
        keys.add((TextView)inflatedView.findViewById(R.id.anti_theft_t9_key_backspace));

        for(TextView view: keys){
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onT9KeyClicked(v.getId());
                }
            });

        }

    }

    private void onT9KeyClicked(int key) {
        switch (key) {
            case R.id.anti_theft_t9_key_0:
                editText.append("0");
                break;
            case R.id.anti_theft_t9_key_1:
                editText.append("1");
                break;
            case R.id.anti_theft_t9_key_2:
                editText.append("2");
                break;
            case R.id.anti_theft_t9_key_3:
                editText.append("3");
                break;
            case R.id.anti_theft_t9_key_4:
                editText.append("4");
                break;
            case R.id.anti_theft_t9_key_5:
                editText.append("5");
                break;
            case R.id.anti_theft_t9_key_6:
                editText.append("6");
                break;
            case R.id.anti_theft_t9_key_7:
                editText.append("7");
                break;
            case R.id.anti_theft_t9_key_8:
                editText.append("8");
                break;
            case R.id.anti_theft_t9_key_9:
                editText.append("9");
                break;
            case R.id.anti_theft_t9_key_backspace: {
                // delete one character
                String passwordStr = editText.getText().toString();
                if (passwordStr.length() > 0) {
                    String newPasswordStr = new StringBuilder(passwordStr)
                            .deleteCharAt(passwordStr.length() - 1).toString();
                    editText.setText(newPasswordStr);
                }
            }
            break;
            case R.id.anti_theft_t9_key_clear:
                editText.setText(null);
                break;
        }
    }

}
