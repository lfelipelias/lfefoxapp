package com.br.lfefox.lfefoxapp.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.br.lfefox.lfefoxapp.R;
import com.br.lfefox.lfefoxapp.constants.MealConstants;
import com.br.lfefox.lfefoxapp.data.MealContract;
import com.br.lfefox.lfefoxapp.fragment.DatePickerDialogFragment;
import com.br.lfefox.lfefoxapp.service.UserService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class ResetPasswordActivity extends AppCompatActivity {

    private View inflatedView;

    private EditText resetUserName;
    private EditText resetBirthday;
    private LinearLayout containerFirstStep;
    private LinearLayout containerSecondStep;

    private boolean mIsReceiverRegistered = false;
    private ResetBroadcastReceiver mReceiver = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        inflatedView = findViewById(R.id.containerReset);

        resetUserName = (EditText) inflatedView.findViewById(R.id.resetUserName);
        resetBirthday = (EditText) inflatedView.findViewById(R.id.resetBirthday);
        containerFirstStep = (LinearLayout) inflatedView.findViewById(R.id.containerFirstStep);

        bindBtnReset();
        bindDatePicker();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!mIsReceiverRegistered) {
            if (mReceiver == null){
                IntentFilter intentF = new IntentFilter(MealConstants.BROADCAST_USER_ACTION);
                mReceiver = new ResetBroadcastReceiver();
                LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(mReceiver, intentF);
                mIsReceiverRegistered = true;
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mIsReceiverRegistered) {
            LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(mReceiver);
            mReceiver = null;
            mIsReceiverRegistered = false;
        }
    }

    private void bindBtnReset(){
        Button btn = (Button)inflatedView.findViewById(R.id.button_reset_pass);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean hasNoErrors = true;

                if(resetUserName.getText().toString().equalsIgnoreCase("")
                        || resetUserName.getText().length() < 5){
                    resetUserName.setError(getResources().getString(R.string.label_mandatory));
                    hasNoErrors = false;
                }

                if(resetBirthday.getText().toString().equalsIgnoreCase("")){
                    resetBirthday.setError(getResources().getString(R.string.label_mandatory));
                    hasNoErrors = false;
                }

                SimpleDateFormat sdf = new SimpleDateFormat(getResources().getString(R.string.date_format_default), Locale.getDefault());
                Date dateBirth = new Date();
                try {
                    dateBirth = sdf.parse(resetBirthday.getText().toString());
                } catch (ParseException e) {
                    Log.e("RegisterFragment", "Error while parsing birthday date");
                }
                if(hasNoErrors){
                    //Calling UserService
                    Intent intent = new Intent(getApplicationContext(), UserService.class);
                    intent.setData(MealContract.UserEntry.CONTENT_URI);
                    intent.putExtra(MealConstants.USER_SERVICE_ACTION, MealConstants.USER_SERVICE_ACTION_RESET_PASS);

                    intent.putExtra(MealConstants.KEY_USER_NAME_EXTRA, resetUserName.getText().toString());
                    intent.putExtra(MealConstants.KEY_BIRTHDAY_EXTRA, dateBirth.getTime());

                    getApplicationContext().startService(intent);
                }

            }
        });

    }

    private void bindDatePicker() {
        resetBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                DatePickerDialogFragment newFragment = new DatePickerDialogFragment();
                newFragment.setInitialDate(1980, 1, 1);
                newFragment.setListener(dateListener);
                newFragment.show(ft, "dialog");

            }
        });
    }


    private void bindSecondStep(){

        Button btn = (Button)inflatedView.findViewById(R.id.buttonConfirmReset);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean hasNoErrors = true;

                EditText registerPass = (EditText)inflatedView.findViewById(R.id.resetPass);
                if(registerPass.getText().toString().equalsIgnoreCase("")
                        || registerPass.getText().length() < 5){
                    registerPass.setError(getResources().getString(R.string.register_hint_size));
                    hasNoErrors = false;
                }

                EditText registerPassConfirmation = (EditText)inflatedView.findViewById(R.id.resetPassConfirmation);
                if(registerPassConfirmation.getText().toString().equalsIgnoreCase("")
                        || registerPassConfirmation.getText().length() < 5){
                    registerPassConfirmation.setError(getResources().getString(R.string.register_hint_size));
                    hasNoErrors = false;
                }



                if(!registerPass.getText().toString().equalsIgnoreCase(registerPassConfirmation.getText().toString())){
                    registerPass.setError(getResources().getString(R.string.password_not_equal));
                    hasNoErrors = false;
                }

                SimpleDateFormat sdf = new SimpleDateFormat(getResources().getString(R.string.date_format_default), Locale.getDefault());
                Date dateBirth = new Date();
                try {
                    dateBirth = sdf.parse(resetBirthday.getText().toString());
                } catch (ParseException e) {
                    Log.e("RegisterFragment", "Error while parsing birthday date");
                }

                if(hasNoErrors){

                    //Calling UserService
                    Intent intent = new Intent(getApplicationContext(), UserService.class);
                    intent.setData(MealContract.UserEntry.CONTENT_URI);
                    intent.putExtra(MealConstants.USER_SERVICE_ACTION, MealConstants.USER_SERVICE_ACTION_RESET_CONFIRMATION);


                    intent.putExtra(MealConstants.KEY_USER_NAME_EXTRA, resetUserName.getText().toString());
                    intent.putExtra(MealConstants.KEY_BIRTHDAY_EXTRA, dateBirth.getTime());
                    intent.putExtra(MealConstants.KEY_ACCESS_KEY_EXTRA, registerPass.getText().toString());

                    getApplicationContext().startService(intent);

                }
            }
        });

    }

    /**
     * Listener for  birthday DatePicker
     */
    DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar myCalendar = Calendar.getInstance();

            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            SimpleDateFormat sdf = new SimpleDateFormat(getResources().getString(R.string.date_format_default), Locale.getDefault());

            resetBirthday.setText(sdf.format(myCalendar.getTime()));
        }

    };

    private void displayErrorDialog(){

        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.other_settings_confirmation);

        TextView textView = (TextView)dialog.findViewById(R.id.labelConfirmationMsg);
        textView.setText(R.string.msg_error_reset_pass);
        ImageView img = (ImageView) dialog.findViewById(R.id.imgConfirmationMsg);
        img.setImageResource(R.drawable.info);
        dialog.show();

    }

    /**
     * This class is responsible for dealing with UserService callbacks
     */
    private class ResetBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {


            //UserService can perform tasks for different fragments
            final int actionPerformed = (int)intent.getExtras().get(MealConstants.USER_SERVICE_ACTION);
            int dataStatus = (int)intent.getExtras().get(MealConstants.EXTENDED_DATA_STATUS_USER);

            if(actionPerformed == MealConstants.USER_SERVICE_ACTION_RESET_PASS){

                callBackResetPass(dataStatus);

            } else if(actionPerformed == MealConstants.USER_SERVICE_ACTION_RESET_CONFIRMATION){

                callBackResetConfirmation(context, dataStatus);

            }

        }

        /**
         * Callback to treat the first step of password reset
         * @param dataStatus
         */
        private void callBackResetPass(int dataStatus){

            if(dataStatus == MealConstants.STATUS_RESET_PASS_OK){

                containerFirstStep.setVisibility(View.GONE);

                containerSecondStep = (LinearLayout) inflatedView.findViewById(R.id.containerSecondStep);
                containerSecondStep.setVisibility(View.VISIBLE);
                bindSecondStep();

            } else if(dataStatus == MealConstants.STATUS_RESET_PASS_NOK){

                resetUserName.setError(getResources().getString(R.string.label_invalid));
                resetBirthday.setError(getResources().getString(R.string.label_invalid));

            }

        }

        /**
         * Callback to treat the second step of password reset
         * @param dataStatus
         */
        private void callBackResetConfirmation (Context context, int dataStatus){

            if(dataStatus == MealConstants.STATUS_RESET_PASS_OK){

                Intent intentLogin = new Intent(context, LoginActivity.class);

                //This flag is used to identify that a reset was done before launching LoginActivity
                intentLogin.putExtra(MealConstants.KEY_RESET_PREVIOUSLY, MealConstants.FLAG_RESET_PREVIOUSLY);
                intentLogin.putExtra(MealConstants.KEY_ADMIN_EXIST, MealConstants.CHECK_ADMIN_EXIST);
                startActivity(intentLogin);
                finish();

            } else if(dataStatus == MealConstants.STATUS_RESET_PASS_NOK){
                displayErrorDialog();
            }

        }
    }
}
