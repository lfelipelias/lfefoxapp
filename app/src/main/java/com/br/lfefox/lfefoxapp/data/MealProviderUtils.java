package com.br.lfefox.lfefoxapp.data;


import java.util.Calendar;
import java.util.Date;

/**
 * This class contain templates for queries and util functions used in MealProvider
 */
public class MealProviderUtils {
    public static final String queryCountMealsByPhoneAndStatus = "SELECT COUNT(*) FROM "+ MealContract.MealEntry.TABLE_NAME + " WHERE "+MealContract.MealEntry.COLUMN_PHONE_NUMBER+" = ? AND "+MealContract.MealEntry.COLUMN_VALID+" = ?";

    public static final String wherePhoneById = MealContract.PhoneEntry.TABLE_NAME+ "."+ MealContract.PhoneEntry._ID + " = ? ";
    public static final String whereMealByPhoneAndDate = MealContract.MealEntry.TABLE_NAME+ "."+MealContract.MealEntry.COLUMN_PHONE_NUMBER +" = ? AND " +MealContract.MealEntry.TABLE_NAME+ "."+ MealContract.MealEntry.COLUMN_CHECKIN_DATE + " BETWEEN ? AND ? " ;
    public static final String whereUserByUsername = MealContract.UserEntry.TABLE_NAME+ "."+MealContract.UserEntry.USER_NAME +" = ? ";
    public static final String whereUserByUsernameBirthDay = MealContract.UserEntry.TABLE_NAME+ "."+MealContract.UserEntry.USER_NAME +" = ? AND " + MealContract.UserEntry.TABLE_NAME+ "."+MealContract.UserEntry.BIRTHDAY +" = ? ";

    public static final String whereMealByPhone = MealContract.MealEntry.TABLE_NAME+ "."+MealContract.MealEntry.COLUMN_PHONE_NUMBER +" = ? ";
    public static final String whereMealByDate = MealContract.MealEntry.TABLE_NAME+ "."+MealContract.MealEntry.COLUMN_CHECKIN_DATE + " BETWEEN ? AND ? ";
    public static final String whereMealByPhoneAndStatus = MealContract.MealEntry.TABLE_NAME+ "."+MealContract.MealEntry.COLUMN_PHONE_NUMBER +" = ? AND "+MealContract.MealEntry.COLUMN_VALID+" = ?";


    public static final String queryLastCheckin = "SELECT "+ MealContract.MealEntry.TABLE_NAME+"."+MealContract.MealEntry.COLUMN_CHECKIN_DATE+
            " FROM "+ MealContract.MealEntry.TABLE_NAME+
            " WHERE "+ MealContract.MealEntry.TABLE_NAME+ "."+ MealContract.MealEntry.COLUMN_PHONE_NUMBER +" = ? "+
            " ORDER BY "+ MealContract.MealEntry.TABLE_NAME+ "."+ MealContract.MealEntry.COLUMN_CHECKIN_DATE +" DESC LIMIT 1";


    public static Date getCalendarWithInitialHour(Calendar calendar){
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        return calendar.getTime();
    }

    public static Date getCalendarWithFinalHour(Calendar calendar){
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        return calendar.getTime();
    }
}
