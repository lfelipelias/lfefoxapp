package com.br.lfefox.lfefoxapp.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import javax.inject.Inject;

/**
 * This class defines table and column names for tables related to MealApp
 */
public class MealDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    public static final String DATABASE_NAME = "meal.db";

    @Inject
    public MealDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        final String SQL_CREATE_PHONE_TABLE = "CREATE TABLE " + MealContract.PhoneEntry.TABLE_NAME + " (" +
                MealContract.PhoneEntry._ID + " LONG PRIMARY KEY," +
                MealContract.PhoneEntry.COLUMN_REGISTER_DATE + " LONG NOT NULL "+
                " );";


        final String SQL_CREATE_MEAL_TABLE = "CREATE TABLE " + MealContract.MealEntry.TABLE_NAME + " (" +
                MealContract.MealEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                MealContract.MealEntry.COLUMN_PHONE_NUMBER + " LONG NOT NULL ,"+
                MealContract.MealEntry.COLUMN_VALID + " INTEGER NOT NULL ,"+
                MealContract.MealEntry.COLUMN_CHECKIN_DATE + " LONG NOT NULL ,"+

                // Set up the location column as a foreign key to location table.
                " FOREIGN KEY (" + MealContract.MealEntry.COLUMN_PHONE_NUMBER + ") REFERENCES " +
                MealContract.PhoneEntry.TABLE_NAME + " ("+ MealContract.PhoneEntry._ID+ ")" +

                " );";




        final String SQL_CREATE_USER_TABLE = "CREATE TABLE "+ MealContract.UserEntry.TABLE_NAME + " ( "+
                MealContract.UserEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                MealContract.UserEntry.NAME + " VARCHAR(40) NOT NULL,"+
                MealContract.UserEntry.USER_NAME + " VARCHAR(15) NOT NULL,"+
                MealContract.UserEntry.BIRTHDAY + " LONG NOT NULL NOT NULL,"+
                MealContract.UserEntry.ACESS_KEY + " VARCHAR(50) NOT NULL" +
                " );";

        db.execSQL(SQL_CREATE_PHONE_TABLE);
        db.execSQL(SQL_CREATE_MEAL_TABLE);
        db.execSQL(SQL_CREATE_USER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + MealContract.PhoneEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + MealContract.MealEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + MealContract.UserEntry.TABLE_NAME);
        onCreate(db);
    }

}
