package com.br.lfefox.lfefoxapp.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.br.lfefox.lfefoxapp.R;
import com.br.lfefox.lfefoxapp.constants.MealConstants;

public class ConfirmationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


    }

    @Override
    protected void onResume() {
        super.onResume();

        String msg = (String)getIntent().getExtras().get(MealConstants.EXTENDED_DATA_RETURN_MESSAGE);
        TextView textConfirmation = (TextView) findViewById(R.id.confirmation_text);
        textConfirmation.setText(msg);

        int dataStatus = (int)getIntent().getExtras().get(MealConstants.EXTENDED_DATA_STATUS);

        ImageView img = (ImageView) findViewById(R.id.imageViewConfirmation);


        if(dataStatus == MealConstants.STATUS_CHECKIN_OK){
            img.setImageResource(R.drawable.chefcooking);

        } else if(dataStatus == MealConstants.STATUS_CHECKIN_NOK){
            img.setImageResource(R.drawable.exclamation);
        }
        else if(dataStatus == MealConstants.STATUS_CHECKIN_PRIZE){
            img.setImageResource(R.drawable.prize);
        }

        Handler handler = new Handler();

        //Message will be displayed for 5 seconds
        handler.postDelayed(new Runnable() {
            public void run() {
                finish();
            }
        }, 5000);
    }
}
