package com.br.lfefox.lfefoxapp.data;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Contract for Meal database
 */
public class MealContract {

    public static final String CONTENT_AUTHORITY = "com.br.lfefox.lfefoxapp";

    // Use CONTENT_AUTHORITY to create the base of all URI's which apps will use to contact
    // the content provider.
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_PHONE = "phone";
    public static final String PATH_MEAL = "meal";
    public static final String PATH_USER = "user";



    public static final class PhoneEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_PHONE).build();
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PHONE;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_PHONE;

        // Table name
        public static final String TABLE_NAME = "phone";
        public static final String COLUMN_REGISTER_DATE = "register_date";

        public static Uri buildPhoneUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static String getPhoneFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }

    }

    public static final class MealEntry implements BaseColumns {
        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_MEAL).build();
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MEAL;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_MEAL;

        // Table name
        public static final String TABLE_NAME = "meal";
        public static final String COLUMN_VALID = "valid";
        public static final String COLUMN_PHONE_NUMBER = "phone_id";
        public static final String COLUMN_CHECKIN_DATE = "checkin_date";

        public static Uri buildMealUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

        public static Uri buildMealWithPhoneNumber(long phoneNumber) {
            return CONTENT_URI.buildUpon().appendPath(Long.toString(phoneNumber)).build();
        }

        public static String getPhoneFromUri(Uri uri) {
            return uri.getPathSegments().get(1);
        }

        public static Uri buildCountMealsByPhoneAndStatus(long phone, int indicator) {
            return CONTENT_URI.buildUpon().appendPath(String.valueOf(phone))
                    .appendPath("COUNT")
                    .appendQueryParameter("ind", String.valueOf(indicator)).build();
        }

        public static Uri builLastMealByPhone(long phone) {
            return CONTENT_URI.buildUpon().appendPath(String.valueOf(phone))
                    .appendPath("LAST").build();
        }

    }
    public static final class UserEntry implements BaseColumns {

        public static final Uri CONTENT_URI = BASE_CONTENT_URI.buildUpon().appendPath(PATH_USER).build();
        public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_USER;
        public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_USER;

        public static final String TABLE_NAME = "user";

        public static final String NAME = "name";
        public static final String USER_NAME = "user_name";
        public static final String BIRTHDAY = "birth";
        public static final String ACESS_KEY = "key";

        public static Uri buildUserUri(long id) {
            return ContentUris.withAppendedId(CONTENT_URI, id);
        }

    }

}
