package com.br.lfefox.lfefoxapp.component;

import com.br.lfefox.lfefoxapp.MainActivity;
import com.br.lfefox.lfefoxapp.data.MealProvider;
import com.br.lfefox.lfefoxapp.module.StorageModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {StorageModule.class})
public interface StorageComponent {
    void inject (MainActivity mainActivity);
    void inject (MealProvider mealProvider);

}
