package com.br.lfefox.lfefoxapp.service;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.br.lfefox.lfefoxapp.R;
import com.br.lfefox.lfefoxapp.constants.MealConstants;
import com.br.lfefox.lfefoxapp.data.MealContract;
import com.br.lfefox.lfefoxapp.data.MealProviderUtils;
import com.br.lfefox.lfefoxapp.to.CheckinTO;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * This Service is responsible to perform background tasks for MealApp
 */
public class MealService extends IntentService {



    public MealService() {
        super("MealService");
    }

    public MealService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        int action = (int)  intent.getExtras().get(MealConstants.MEAL_SERVICE_ACTION);

        switch (action) {

            case MealConstants.MEAL_SERVICE_ACTION_CHECKIN:
            {
                actionCheckin(intent);
                break;
            } case MealConstants.MEAL_SERVICE_ACTION_FIND_MEALS:
            {
                findMeals(intent);
            } case MealConstants.MEAL_SERVICE_ACTION_FIND_CLIENT_INFO:
            {
                findClientInfo(intent);
            }
        }

    }

    private void findMeals(Intent intent){
        Long dtInit = null;
        Long dtEnd = null;
        String phoneNumber = null;

        String [] selectionCheckin = null;
        String where = null;

        if(intent.getExtras().get(MealConstants.KEY_DT_INIT) != null ){
            dtInit = (Long)intent.getExtras().get(MealConstants.KEY_DT_INIT);
        }

        if(intent.getExtras().get(MealConstants.KEY_DT_END) != null ){
            dtEnd = (Long)intent.getExtras().get(MealConstants.KEY_DT_END);
        }

        if(intent.getExtras().get(MealConstants.KEY_PHONE_NUMBER) != null ){
            phoneNumber = (String)intent.getExtras().get(MealConstants.KEY_PHONE_NUMBER);
        }

        if(phoneNumber != null && dtInit != null){

            //User informed phone number and date range
            selectionCheckin = new String[]{String.valueOf(phoneNumber), String.valueOf(dtInit), String.valueOf(dtEnd)};
            where = MealProviderUtils.whereMealByPhoneAndDate;

        } else if(phoneNumber != null){
            //User informed only phone number
            selectionCheckin = new String[]{String.valueOf(phoneNumber)};
            where = MealProviderUtils.whereMealByPhone;

        } else if(dtInit != null){
            //User informed only date range
            selectionCheckin = new String[]{String.valueOf(dtInit), String.valueOf(dtEnd)};
            where = MealProviderUtils.whereMealByDate;

        }

        Cursor cursor = getApplicationContext().getContentResolver().query(MealContract.MealEntry.CONTENT_URI, null, where, selectionCheckin, null);

        List<CheckinTO> list = parseMeals(cursor);

        Intent localIntent = new Intent(MealConstants.BROADCAST_ACTION);
        localIntent.putExtra(MealConstants.MEAL_SERVICE_ACTION, MealConstants.MEAL_SERVICE_ACTION_FIND_MEALS);
        localIntent.putExtra(MealConstants.KEY_LIST_CHECKINS, (Serializable) list);
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);


        cursor.close();

    }

    private List<CheckinTO> parseMeals(Cursor cursor){
        List<CheckinTO> list = new ArrayList<>();

        while(cursor.moveToNext()){

            String status = cursor.getInt(2)==MealConstants.STATUS_CHECKIN_VALID ?
                getResources().getString(R.string.active):getResources().getString(R.string.inactive);

            Date dateCheckin = new Date(cursor.getLong(3));
            SimpleDateFormat sdf = new SimpleDateFormat(getResources().getString(R.string.date_format_default), Locale.getDefault());


            CheckinTO checkinTO = new CheckinTO();
                checkinTO.setId(cursor.getInt(0));
                checkinTO.setPhoneNumber(cursor.getString(1));
                checkinTO.setStatus(status);
                checkinTO.setCheckinDate(sdf.format(dateCheckin));

            list.add(checkinTO);
        }

        return list;
    }

    private void actionCheckin(Intent intent){

        String phoneFromUri = MealContract.MealEntry.getPhoneFromUri(intent.getData());

        if(canUserDoCheckin(Long.valueOf(phoneFromUri))){

            checkPhoneNumber(phoneFromUri);
            perFormCheckin(phoneFromUri);

        } else {
            //User can do only 1 checkin per day
            String msgReturn = getResources().getString(R.string.msg_checkin_already_done);

            //Setting the response to the MealBroadCastReceiver
            Intent localIntent = new Intent(MealConstants.BROADCAST_ACTION).putExtra(MealConstants.EXTENDED_DATA_STATUS, MealConstants.STATUS_CHECKIN_NOK);
            localIntent.putExtra(MealConstants.MEAL_SERVICE_ACTION, MealConstants.MEAL_SERVICE_ACTION_CHECKIN);
            localIntent.putExtra(MealConstants.EXTENDED_DATA_RETURN_MESSAGE, msgReturn);
            LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
        }

    }

    /**
     * Methodo to check if user already did a checkin today
     * @return
     */
    private boolean canUserDoCheckin(long phoneNumber){
        //CHECK if user already did a check-in today
        Date dateBegin = MealProviderUtils.getCalendarWithInitialHour(Calendar.getInstance());
        Date dateEnd = MealProviderUtils.getCalendarWithFinalHour(Calendar.getInstance());
        final String [] selectionCheckin = new String[]{String.valueOf(phoneNumber), String.valueOf(dateBegin.getTime()), String.valueOf(dateEnd.getTime())};

        Cursor cursorChecking = getApplicationContext().getContentResolver().query(MealContract.MealEntry.CONTENT_URI, null, MealProviderUtils.whereMealByPhoneAndDate, selectionCheckin, null);
        //END CHECK if user already did a check-in today

        cursorChecking.close();

        return cursorChecking.getCount() == 0;
    }

    private void checkPhoneNumber(String phoneNumber){

        //check if phoneNumber exists in PHONE table and insert
        ContentValues insertPhone = new ContentValues();
        insertPhone.put(MealContract.PhoneEntry._ID, phoneNumber);
        insertPhone.put(MealContract.PhoneEntry.COLUMN_REGISTER_DATE, new Date().getTime());

        Cursor phone = getApplicationContext().getContentResolver().query(
                MealContract.PhoneEntry.buildPhoneUri(11995765884l), null,
                MealProviderUtils.wherePhoneById,  new String[]{phoneNumber}, null);

        if(phone.getCount() == 0){
            //INSERT PHONE if it does not exist
            getApplicationContext().getContentResolver().insert(MealContract.PhoneEntry.CONTENT_URI, insertPhone);
        }
        phone.close();

    }

    private void perFormCheckin(String phoneNumber){

        String msgReturn = null;

        //Check the number of rows for valid check-ins in Meal table
        Uri countUri = MealContract.MealEntry.buildCountMealsByPhoneAndStatus(Long.valueOf(phoneNumber), MealConstants.CHECKIN_VALID);
        Cursor cursorCount = getApplicationContext().getContentResolver().query(countUri, null, null, null, null);

        int returnCode = MealConstants.STATUS_CHECKIN_OK;

        int count = 0;
        if(cursorCount.getCount() > 0){
            cursorCount.moveToFirst();
            count = cursorCount.getInt(0);
            cursorCount.close();
        }

        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

        int defaultCounter =  prefs.getInt(MealConstants.KEY_PREF_CHECKIN_NUMBER, MealConstants.DEFAULT_CHECKIN_NUMBER);
        String serviceOffered = prefs.getString(MealConstants.KEY_PREF_SERVICE_NAME, getResources().getString(R.string.label_default_service_name));

        if(count < defaultCounter) {

            ContentValues insertMeal = new ContentValues();
            insertMeal.put(MealContract.MealEntry.COLUMN_PHONE_NUMBER, phoneNumber);
            insertMeal.put(MealContract.MealEntry.COLUMN_CHECKIN_DATE, new Date().getTime());
            insertMeal.put(MealContract.MealEntry.COLUMN_VALID, MealConstants.CHECKIN_VALID);

            try {
                //INSERT meal
                getApplicationContext().getContentResolver().insert(MealContract.MealEntry.CONTENT_URI, insertMeal);
            } catch (Exception e) {
                Log.e("MealService", "error when inserting meal");
            }
            ++count;
            if(count == defaultCounter) {
                msgReturn = getResources().getString(R.string.msg_before_get_lunch, count, serviceOffered);
            } else {
                msgReturn = getResources().getString(R.string.msg_get_lunch, count, defaultCounter, serviceOffered);
            }
        } else {
            //It is the free meal day, time to update the COLUMN_VALID in Meal table
            ContentValues updateMealValues = new ContentValues();

            updateMealValues.put(MealContract.MealEntry.COLUMN_VALID, MealConstants.CHECKIN_INVALID);

            String where = MealContract.MealEntry.TABLE_NAME+ "."+ MealContract.MealEntry.COLUMN_PHONE_NUMBER + " = ? AND " + MealContract.MealEntry.TABLE_NAME+ "."+MealContract.MealEntry.COLUMN_VALID + " = ?";

            int countUpdates = getApplicationContext().getContentResolver().update(MealContract.MealEntry.CONTENT_URI, updateMealValues, where,  new String[]{phoneNumber, String.valueOf(MealConstants.CHECKIN_VALID)});

            msgReturn = getResources().getString(R.string.msg_free_lunch, count, serviceOffered);
            returnCode = MealConstants.STATUS_CHECKIN_PRIZE;
        }

        //Setting the response to the MealBroadCastReceiver
        Intent localIntent = new Intent(MealConstants.BROADCAST_ACTION).putExtra(MealConstants.EXTENDED_DATA_STATUS, returnCode);
        localIntent.putExtra(MealConstants.EXTENDED_DATA_RETURN_MESSAGE, msgReturn);
        localIntent.putExtra(MealConstants.MEAL_SERVICE_ACTION, MealConstants.MEAL_SERVICE_ACTION_CHECKIN);
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }

    private void findClientInfo(Intent intent){

        //Setting the response to the MealBroadCastReceiver
        Intent localIntent = new Intent(MealConstants.BROADCAST_ACTION).putExtra(MealConstants.EXTENDED_DATA_STATUS, MealConstants.STATUS_CHECKIN_NOK);
        localIntent.putExtra(MealConstants.MEAL_SERVICE_ACTION, MealConstants.MEAL_SERVICE_ACTION_FIND_CLIENT_INFO);


        String phoneNumber = MealContract.MealEntry.getPhoneFromUri(intent.getData());

        String[] selectionCheckin = new String[]{phoneNumber};
        Cursor cursorCountAll = getApplicationContext().getContentResolver().query(MealContract.MealEntry.CONTENT_URI,
                null, MealProviderUtils.whereMealByPhone, selectionCheckin, null);

        if(cursorCountAll.getCount() > 0){

            //Check the number of rows for valid check-ins in Meal table
            Uri countUri = MealContract.MealEntry.buildCountMealsByPhoneAndStatus(Long.valueOf(phoneNumber), MealConstants.CHECKIN_VALID);
            Cursor cursorCountValid = getApplicationContext().getContentResolver().query(countUri, null, null, null, null);

            int countValid = 0;
            if(cursorCountValid.getCount() > 0) {
                cursorCountValid.moveToFirst();
                countValid = cursorCountValid.getInt(0);
            }


            Uri countUriLast = MealContract.MealEntry.builLastMealByPhone(Long.valueOf(phoneNumber));
            Cursor cursorLastCheckin = getApplicationContext().getContentResolver().query(countUriLast, null, null, null, null);

            cursorLastCheckin.moveToFirst();

            Date dateCheckin = new Date(cursorLastCheckin.getLong(0));
            SimpleDateFormat sdf = new SimpleDateFormat(getResources().getString(R.string.date_format_with_hour), Locale.getDefault());
            String lastDateCheckin = sdf.format(dateCheckin);


            localIntent.putExtra(MealConstants.KEY_COUNT_ALL_CHECKINS, cursorCountAll.getCount());
            localIntent.putExtra(MealConstants.KEY_COUNT_VALID_CHECKINS, countValid);
            localIntent.putExtra(MealConstants.KEY_LAST_DATE_CHECKIN, lastDateCheckin);



            cursorCountAll.close();
            cursorCountValid.close();
            cursorLastCheckin.close();

        } else {
            //Phone number doesnt exist in meal table
            localIntent.putExtra(MealConstants.KEY_PHONE_NOT_FOUND, Boolean.TRUE);
            cursorCountAll.close();

        }

        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }
}
