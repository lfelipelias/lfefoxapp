package com.br.lfefox.lfefoxapp.fragment;

import android.app.Dialog;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.br.lfefox.lfefoxapp.R;
import com.br.lfefox.lfefoxapp.activity.CheckinActivity;
import com.br.lfefox.lfefoxapp.activity.LoginActivity;
import com.br.lfefox.lfefoxapp.constants.MealConstants;
import com.br.lfefox.lfefoxapp.data.MealContract;
import com.br.lfefox.lfefoxapp.service.UserService;


public class WelcomeFragment extends Fragment {

    private View inflatedView;
    private boolean mIsReceiverRegistered = false;
    private WelcomeBroadcastReceiver mReceiver = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        inflatedView = inflater.inflate(R.layout.welcome_fragment_main, container, false);

        return inflatedView;
    }


    @Override
    public void onResume() {
        super.onResume();

        checkRegister();

        //Calling UserService to check if there is an admin user registered
        Intent intent = new Intent(getActivity().getApplicationContext(), UserService.class);
        intent.setData(MealContract.UserEntry.CONTENT_URI);
        intent.putExtra(MealConstants.USER_SERVICE_ACTION, MealConstants.USER_SERVICE_ACTION_CHECK_ADMIN);
        getActivity().getApplicationContext().startService(intent);

        if (!mIsReceiverRegistered) {
            if (mReceiver == null){
                IntentFilter intentF = new IntentFilter(MealConstants.BROADCAST_USER_ACTION);
                mReceiver = new WelcomeBroadcastReceiver();
                LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).registerReceiver(mReceiver, intentF);
                mIsReceiverRegistered = true;
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //Removing our MealBroadcastReceiver
        if (mIsReceiverRegistered) {
            LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mReceiver);
            mReceiver = null;
            mIsReceiverRegistered = false;
        }
    }

    /**
     * If the user just created an account a message will be displayed when he is redirected to
     * WelcomeFragment
     */
    private void checkRegister(){
        if(getActivity().getIntent().getExtras() != null
                && getActivity().getIntent().getExtras().get(MealConstants.KEY_REGISTER_CREATED) != null){

            Dialog dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.other_settings_confirmation);

            TextView textView = (TextView)dialog.findViewById(R.id.labelConfirmationMsg);
            textView.setText(R.string.msg_success_registering);
            ImageView img = (ImageView) dialog.findViewById(R.id.imgConfirmationMsg);
            img.setImageResource(R.drawable.checkedmark);
            dialog.show();
        }
    }

    /**
     * This broadcast is responsible for checking if there is an admin user for the app
     * in order to show the proper activity
     */
    private class WelcomeBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            //UserService can perform tasks for different fragments
            final int actionPerformed = (int)intent.getExtras().get(MealConstants.USER_SERVICE_ACTION);

            if(actionPerformed == MealConstants.USER_SERVICE_ACTION_CHECK_ADMIN){
                checkUserExistReturn(context, intent);
            }

        }

        private void checkUserExistReturn(Context context, Intent intent){
            final int userExists = (int)intent.getExtras().get(MealConstants.EXTENDED_DATA_STATUS_USER);

            Button btnRegister = (Button) inflatedView.findViewById(R.id.welcome_fragment_login);
            btnRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), LoginActivity.class);

                    //This information identify if login or register fragment must be inflated
                    intent.putExtra(MealConstants.KEY_ADMIN_EXIST, userExists);
                    startActivity(intent);

                }
            });

            if(userExists == MealConstants.CHECK_ADMIN_EXIST) {
                //User must be registered to use the app, now OK button and login is displayed
                btnRegister.setText(R.string.welcome_Login_button_text);

                //Configuring OK button
                Button btn = (Button) inflatedView.findViewById(R.id.welcome_fragment_ok);
                btn.setVisibility(View.VISIBLE);
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), CheckinActivity.class);
                        startActivity(intent);
                    }
                });

            }
        }
    }

}
