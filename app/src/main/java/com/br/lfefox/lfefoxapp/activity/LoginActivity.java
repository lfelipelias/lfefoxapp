package com.br.lfefox.lfefoxapp.activity;

import android.app.Dialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.br.lfefox.lfefoxapp.MainActivity;
import com.br.lfefox.lfefoxapp.R;
import com.br.lfefox.lfefoxapp.constants.MealConstants;
import com.br.lfefox.lfefoxapp.fragment.LoginFragment;
import com.br.lfefox.lfefoxapp.fragment.RegisterFragment;

public class LoginActivity extends AppCompatActivity {

    private LinearLayout linearLayout;
    private FragmentManager fm;
    private boolean mIsReceiverRegistered = false;
    private UserBroadcastReceiver mReceiver = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        linearLayout = (LinearLayout)findViewById(R.id.linear_login_area);
        fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        int userExists = (int) getIntent().getExtras().get(MealConstants.KEY_ADMIN_EXIST);

        if(userExists == MealConstants.CHECK_ADMIN_DOESNT_EXIST){
            ft.add(R.id.linear_login_area, new RegisterFragment());
        } else {
            ft.add(R.id.linear_login_area, new LoginFragment());
        }

        ft.commit();

        checkPreviousReset();

    }

    /**
     * When user resets the password he is redirected to loginActivity in those cases
     * a message confirming the change is shown
     */
    private void checkPreviousReset(){
        if(getIntent().getExtras().get(MealConstants.KEY_RESET_PREVIOUSLY) != null){

            Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.other_settings_confirmation);

            TextView textView = (TextView)dialog.findViewById(R.id.labelConfirmationMsg);
            textView.setText(R.string.msg_reset_password);
            ImageView img = (ImageView) dialog.findViewById(R.id.imgConfirmationMsg);
            img.setImageResource(R.drawable.checkedmark);
            dialog.show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!mIsReceiverRegistered) {
            if (mReceiver == null){
                IntentFilter intentF = new IntentFilter(MealConstants.BROADCAST_USER_ACTION);
                mReceiver = new UserBroadcastReceiver();
                LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, intentF);
                mIsReceiverRegistered = true;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Removing our MealBroadcastReceiver
        if (mIsReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
            mReceiver = null;
            mIsReceiverRegistered = false;
        }
    }

    private void displayInfoDialog(int idText, int idImg){

        Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.other_settings_confirmation);

        TextView textView = (TextView)dialog.findViewById(R.id.labelConfirmationMsg);
        textView.setText(idText);
        ImageView img = (ImageView) dialog.findViewById(R.id.imgConfirmationMsg);
        img.setImageResource(idImg);
        dialog.show();

    }
    private class UserBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            //UserService can perform tasks for different fragments
            final int actionPerformed = (int)intent.getExtras().get(MealConstants.USER_SERVICE_ACTION);

            if(actionPerformed == MealConstants.USER_SERVICE_ACTION_REGISTER){

                int dataStatus = (int)intent.getExtras().get(MealConstants.EXTENDED_DATA_STATUS_USER);

                if(dataStatus == MealConstants.STATUS_REGISTER_OK){

                    Intent intentMain = new Intent(getApplicationContext(), MainActivity.class);
                    intentMain.putExtra(MealConstants.KEY_REGISTER_CREATED, MealConstants.STATUS_REGISTER_CREATED);
                    startActivity(intentMain);
                    finish();
                } else if(dataStatus == MealConstants.STATUS_REGISTER_NOK){
                    displayInfoDialog(R.string.msg_error_registering, R.drawable.info);

                }

            }

        }
    }
}
