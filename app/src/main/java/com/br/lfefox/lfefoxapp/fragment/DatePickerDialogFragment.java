package com.br.lfefox.lfefoxapp.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

import com.br.lfefox.lfefoxapp.R;

/**
 * Class used to show datepicker
 */
public class DatePickerDialogFragment extends DialogFragment {

    private DatePickerDialog.OnDateSetListener listener;
    private int year;
    private int month;
    private int day;
    public DatePickerDialogFragment() { super();}

    public Dialog onCreateDialog(Bundle savedInstanceState) {


        return new DatePickerDialog(getActivity(), R.style.DialogTheme, listener, year, month, day);
    }

    public void setListener(DatePickerDialog.OnDateSetListener listener) {
        this.listener = listener;
    }

    public void setInitialDate(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

}
