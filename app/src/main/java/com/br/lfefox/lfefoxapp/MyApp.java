package com.br.lfefox.lfefoxapp;

import android.app.Application;

import com.br.lfefox.lfefoxapp.component.DaggerStorageComponent;
import com.br.lfefox.lfefoxapp.component.StorageComponent;
import com.br.lfefox.lfefoxapp.module.StorageModule;

public class MyApp extends Application{

    StorageComponent component;



    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerStorageComponent
                .builder()
                .storageModule(new StorageModule(this))
                .build();



    }

    public StorageComponent getComponent(){ return component;}
}
