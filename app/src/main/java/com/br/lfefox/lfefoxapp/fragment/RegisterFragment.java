package com.br.lfefox.lfefoxapp.fragment;


import android.app.DatePickerDialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.br.lfefox.lfefoxapp.R;
import com.br.lfefox.lfefoxapp.constants.MealConstants;
import com.br.lfefox.lfefoxapp.data.MealContract;
import com.br.lfefox.lfefoxapp.service.UserService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Register fragment
 */
public class RegisterFragment extends Fragment {

    private View inflatedView;
    private EditText birthdayText;

    public RegisterFragment() {}


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        inflatedView = inflater.inflate(R.layout.fragment_register, container, false);

        birthdayText = (EditText)inflatedView.findViewById(R.id.registerBirthday);
        birthdayText.setInputType(InputType.TYPE_NULL);
        bindDatePicker();
        bindRegisterButton();

        return inflatedView;
    }

    private void bindDatePicker() {
        birthdayText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                DatePickerDialogFragment newFragment = new DatePickerDialogFragment();
                //Initialize datepicker
                newFragment.setInitialDate(1980, 1, 1);
                newFragment.setListener(dateListener);
                newFragment.show(ft, "dialog");

            }
        });
    }

    /**
     * Listener for  birthday DatePicker
     */
    DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            Calendar myCalendar = Calendar.getInstance();

            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

            SimpleDateFormat sdf = new SimpleDateFormat(getResources().getString(R.string.date_format_default), Locale.getDefault());

            birthdayText.setText(sdf.format(myCalendar.getTime()));
        }

    };

    private void bindRegisterButton() {

        Button btn = (Button)inflatedView.findViewById(R.id.button_register);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Check for blank fields

                boolean hasNoErrors = true;

                EditText registerName = (EditText)inflatedView.findViewById(R.id.registerName);
                if(registerName.getText().toString().equalsIgnoreCase("")
                        || registerName.getText().length() < 5){
                    registerName.setError(getResources().getString(R.string.register_hint_size));
                    hasNoErrors = false;
                }

                EditText registerLoginName = (EditText)inflatedView.findViewById(R.id.registerLoginName);
                if(registerLoginName.getText().toString().equalsIgnoreCase("")
                        || registerLoginName.getText().length() < 5){
                    registerLoginName.setError(getResources().getString(R.string.register_hint_size));
                    hasNoErrors = false;
                }

                EditText registerPass = (EditText)inflatedView.findViewById(R.id.registerPass);
                if(registerPass.getText().toString().equalsIgnoreCase("")
                        || registerPass.getText().length() < 5){
                    registerPass.setError(getResources().getString(R.string.register_hint_size));
                    hasNoErrors = false;
                }

                EditText registerPassConfirmation = (EditText)inflatedView.findViewById(R.id.registerPassConfirmation);
                if(registerPassConfirmation.getText().toString().equalsIgnoreCase("")
                        || registerPassConfirmation.getText().length() < 5){
                    registerPassConfirmation.setError(getResources().getString(R.string.register_hint_size));
                    hasNoErrors = false;
                }

                EditText registerBirthday = (EditText)inflatedView.findViewById(R.id.registerBirthday);
                if(registerBirthday.getText().toString().equalsIgnoreCase("")){
                    registerBirthday.setError(getResources().getString(R.string.label_mandatory));
                    hasNoErrors = false;
                }

                if(!registerPass.getText().toString().equalsIgnoreCase(registerPassConfirmation.getText().toString())){
                    registerPass.setError(getResources().getString(R.string.password_not_equal));
                    hasNoErrors = false;
                }

                SimpleDateFormat sdf = new SimpleDateFormat(getResources().getString(R.string.date_format_default), Locale.getDefault());
                Date dateBirth = new Date();
                try {
                    dateBirth = sdf.parse(registerBirthday.getText().toString());
                } catch (ParseException e) {
                    Log.e("RegisterFragment", "Error while parsing birthday date");
                }
                if(hasNoErrors){
                    //Calling UserService
                    Intent intent = new Intent(getActivity().getApplicationContext(), UserService.class);
                    intent.setData(MealContract.UserEntry.CONTENT_URI);
                    intent.putExtra(MealConstants.USER_SERVICE_ACTION, MealConstants.USER_SERVICE_ACTION_REGISTER);

                    intent.putExtra(MealConstants.KEY_NAME_EXTRA, registerName.getText().toString());
                    intent.putExtra(MealConstants.KEY_USER_NAME_EXTRA, registerLoginName.getText().toString());
                    intent.putExtra(MealConstants.KEY_BIRTHDAY_EXTRA, dateBirth.getTime());
                    intent.putExtra(MealConstants.KEY_ACCESS_KEY_EXTRA, registerPass.getText().toString());

                    getActivity().getApplicationContext().startService(intent);
                }

            }
        });
    }


}
