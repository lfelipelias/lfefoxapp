package com.br.lfefox.lfefoxapp.activity;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.br.lfefox.lfefoxapp.MainActivity;
import com.br.lfefox.lfefoxapp.R;
import com.br.lfefox.lfefoxapp.constants.MealConstants;
import com.br.lfefox.lfefoxapp.fragment.ClientFragment;
import com.br.lfefox.lfefoxapp.fragment.OtherSettingsFragment;
import com.br.lfefox.lfefoxapp.fragment.ReportFragment;
import com.br.lfefox.lfefoxapp.fragment.WelcomeAdminFragment;

public class AdminActivity extends AppCompatActivity {

    private FragmentManager fm;

    private DrawerLayout drawerLayout;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private ListView listOptions;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerRowsAdapter drawerRowsAdapter;
    private int currentPage = -1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);


        mTitle = mDrawerTitle = getTitle();

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        listOptions = (ListView) findViewById(R.id.left_drawer);

        // Set the adapter for the list view
        drawerRowsAdapter = new DrawerRowsAdapter(this);
        listOptions.setAdapter(drawerRowsAdapter);

        listOptions.setOnItemClickListener(new DrawerItemClickListener());


        mDrawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getSupportActionBar().setTitle(mTitle);
            }


            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getSupportActionBar().setTitle(mDrawerTitle);
            }

            @Override
            public void onConfigurationChanged(Configuration newConfig) {
                super.onConfigurationChanged(newConfig);
            }
        };

        // Set the drawer toggle as the DrawerListener
        drawerLayout.setDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        //Key used to restore fragment which was previously opened in case of rotation
        if(savedInstanceState != null
                && savedInstanceState.containsKey(MealConstants.KEY_CURRENT_PAGE_ADMIN)){
            currentPage = (int) savedInstanceState.get(MealConstants.KEY_CURRENT_PAGE_ADMIN);
        }

        if(currentPage == -1) {
            fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();
            ft.replace(R.id.content_frame, new WelcomeAdminFragment());
            ft.commit();
        } else {
            selectItem(currentPage);
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(MealConstants.KEY_CURRENT_PAGE_ADMIN, currentPage);
        super.onSaveInstanceState(outState);
    }


    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            currentPage = position;
            selectItem(position);
        }
    }

    private void selectItem(int position) {

        if(position == 3){
            //Exit selection, redirect to main activity
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        } else {

            // Create a new fragment and specify the planet to show based on position
            fm = getFragmentManager();
            FragmentTransaction ft = fm.beginTransaction();

            if (position == 0) {
                ft.replace(R.id.content_frame, new ReportFragment());
            } else if(position == 1){
                ft.replace(R.id.content_frame, new ClientFragment());
            } else {
                ft.replace(R.id.content_frame, new OtherSettingsFragment());
            }

            ft.commit();


            // Highlight the selected item, update the title, and close the drawer
            listOptions.setItemChecked(position, true);

            setTitle(drawerRowsAdapter.getOptionsDrawer()[position]);
            drawerLayout.closeDrawer(listOptions);
        }

    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }


    /**
     * Adapter used to fill the Drawer layout with options and icons
     */
    private class DrawerRowsAdapter extends BaseAdapter {

        private Context context;
        private String [] optionsDrawer;

        private int [] imagesSrc = {R.drawable.seo, R.drawable.client, R.drawable.plus,  R.drawable.exit};

        public DrawerRowsAdapter(Context context){
            this.context = context;
            optionsDrawer = context.getResources().getStringArray(R.array.drawerEntries);
        }

        @Override
        public int getCount() {
            return optionsDrawer.length;
        }

        @Override
        public Object getItem(int position) {
            return optionsDrawer[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row;

            if(convertView == null){
                LayoutInflater inflater = (LayoutInflater) context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                row = inflater.inflate(R.layout.row_drawer, null);

            } else {
                row = convertView;
            }

            ImageView img = (ImageView) row.findViewById(R.id.imgRowDrawer);
            TextView text = (TextView)row.findViewById(R.id.txtRowDrawer);

            text.setText(optionsDrawer[position]);
            img.setImageResource(imagesSrc[position]);

            return row;
        }

        public String[] getOptionsDrawer() {
            return optionsDrawer;
        }
    }
}
