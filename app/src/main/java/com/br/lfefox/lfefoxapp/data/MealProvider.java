package com.br.lfefox.lfefoxapp.data;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.br.lfefox.lfefoxapp.MyApp;

import javax.inject.Inject;

/**
 * Content provider for MealContract
 */
public class MealProvider extends ContentProvider {

    private static final UriMatcher sUriMatcher = buildUriMatcher();

    @Inject
    MealDbHelper mOpenHelper;

    static final int MEAL = 100;
    static final int PHONE = 102;
    static final int MEAL_COUNT = 103;
    static final int USER = 104;
    static final int LAST_CHECKIN = 105;

    private static final SQLiteQueryBuilder sPhoneQueryBuilder;
    static{
        sPhoneQueryBuilder = new SQLiteQueryBuilder();
        sPhoneQueryBuilder.setTables(MealContract.PhoneEntry.TABLE_NAME);
    }
    private static final SQLiteQueryBuilder sMealQueryBuilder;
    static{
        sMealQueryBuilder = new SQLiteQueryBuilder();
        sMealQueryBuilder.setTables(MealContract.MealEntry.TABLE_NAME);
    }

    private static final SQLiteQueryBuilder sUserQueryBuilder;
    static{
        sUserQueryBuilder = new SQLiteQueryBuilder();
        sUserQueryBuilder.setTables(MealContract.UserEntry.TABLE_NAME);
    }

    @Override
    public boolean onCreate() {
        //Content Provider onCreate is called before MyApp.onCreate
        if(((MyApp)getContext().getApplicationContext()).getComponent() == null){
            ((MyApp)getContext().getApplicationContext()).onCreate();
        }
        ((MyApp)getContext().getApplicationContext()).getComponent().inject(this);
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        Cursor retCursor = null;
        switch (sUriMatcher.match(uri)) {

            case PHONE:
            {

                retCursor =   sPhoneQueryBuilder.query(mOpenHelper.getReadableDatabase(),
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);

                break;
            }

            case MEAL:
            {

                retCursor =   sMealQueryBuilder.query(mOpenHelper.getReadableDatabase(),
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);

                break;
            }

            case MEAL_COUNT:
            {

                selectionArgs =  new String[]{MealContract.MealEntry.getPhoneFromUri(uri), uri.getQueryParameter("ind")};
                retCursor = mOpenHelper.getReadableDatabase()
                        .rawQuery(MealProviderUtils.queryCountMealsByPhoneAndStatus, selectionArgs);
                break;
            }

            case USER:
            {

                retCursor =   sUserQueryBuilder.query(mOpenHelper.getReadableDatabase(),
                        projection,
                        selection,
                        selectionArgs,
                        null,
                        null,
                        sortOrder);

                break;
            }

            case LAST_CHECKIN:{

                selectionArgs =  new String[]{MealContract.MealEntry.getPhoneFromUri(uri)};
                retCursor = mOpenHelper.getReadableDatabase()
                        .rawQuery(MealProviderUtils.queryLastCheckin, selectionArgs);
                break;

            }

        }

        if(retCursor != null){
            retCursor.setNotificationUri(getContext().getContentResolver(), uri);
        }

        return retCursor;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        Uri returnUri;

        long _id;
        switch (match) {
            case MEAL: {

                _id = db.insert(MealContract.MealEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = MealContract.MealEntry.buildMealUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case PHONE: {
                _id = db.insert(MealContract.PhoneEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = MealContract.PhoneEntry.buildPhoneUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            case USER: {
                _id = db.insert(MealContract.UserEntry.TABLE_NAME, null, values);
                if ( _id > 0 )
                    returnUri = MealContract.UserEntry.buildUserUri(_id);
                else
                    throw new android.database.SQLException("Failed to insert row into " + uri);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        db.close();
        getContext().getContentResolver().notifyChange(uri, null);
        return returnUri;

    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {


        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {


        final SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int updated = 0;

        switch (match) {
            case MEAL: {

                updated = db.update(MealContract.MealEntry.TABLE_NAME, values, selection, selectionArgs);

                break;
            }
            case PHONE: {

                break;
            }case USER: {
                updated = db.update(MealContract.UserEntry.TABLE_NAME, values, selection, selectionArgs);
                break;
            }
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }

        db.close();

        return updated;
    }

    static UriMatcher buildUriMatcher() {

        final UriMatcher matcher = new UriMatcher(UriMatcher.NO_MATCH);
        final String authority = MealContract.CONTENT_AUTHORITY;

        matcher.addURI(authority, MealContract.PATH_MEAL, MEAL);
        matcher.addURI(authority, MealContract.PATH_PHONE, PHONE);
        matcher.addURI(authority, MealContract.PATH_USER, USER);
        matcher.addURI(authority, MealContract.PATH_PHONE + "/*", PHONE);
        matcher.addURI(authority, MealContract.PATH_MEAL+ "/*/COUNT", MEAL_COUNT);
        matcher.addURI(authority, MealContract.PATH_MEAL+ "/*/LAST", LAST_CHECKIN);


        return matcher;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);

        switch (match) {
            case MEAL:
                return MealContract.MealEntry.CONTENT_TYPE;
            case PHONE:
                return MealContract.MealEntry.CONTENT_TYPE;
            case USER:
                return MealContract.UserEntry.CONTENT_TYPE;
            case MEAL_COUNT:
                return MealContract.MealEntry.CONTENT_TYPE;
            case LAST_CHECKIN:
                return MealContract.MealEntry.CONTENT_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }
}
