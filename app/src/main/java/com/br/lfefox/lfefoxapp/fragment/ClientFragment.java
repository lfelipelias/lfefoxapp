package com.br.lfefox.lfefoxapp.fragment;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.br.lfefox.lfefoxapp.R;
import com.br.lfefox.lfefoxapp.constants.MealConstants;
import com.br.lfefox.lfefoxapp.data.MealContract;
import com.br.lfefox.lfefoxapp.service.MealService;

public class ClientFragment extends Fragment {

    private View inflatedView;
    private Button btnSearch;
    private EditText phoneNumberField;
    private LinearLayout userInforContainer;
    private LinearLayout containerUserNotFound;
    private boolean mIsReceiverRegistered = false;
    private ClientBroadcastReceiver mReceiver = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        inflatedView = inflater.inflate(R.layout.fragment_client, container, false);
        setupBinds();
        return inflatedView;
    }

    private void setupBinds(){
        btnSearch = (Button)inflatedView.findViewById(R.id.btnClientSearch);
        phoneNumberField = (EditText)inflatedView.findViewById(R.id.phone_number_client);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(phoneNumberField.getText().toString().length() >= MealConstants.MIN_SIZE_PHONE_NUMBER){

                    Intent intent = new Intent(getActivity().getApplicationContext(), MealService.class);
                    intent.setData(MealContract.MealEntry.buildMealUri(Long.valueOf(phoneNumberField.getText().toString())));
                    intent.putExtra(MealConstants.MEAL_SERVICE_ACTION, MealConstants.MEAL_SERVICE_ACTION_FIND_CLIENT_INFO);
                    getActivity().getApplicationContext().startService(intent);

                } else {
                    phoneNumberField.setError(getResources().getString(R.string.label_phone_size_invalid, MealConstants.MIN_SIZE_PHONE_NUMBER));
                }

            }
        });

    }



    @Override
    public void onResume() {
        super.onResume();

        //Registering our MealBroadcastReceiver to receive call after MealService is done with its work
        if (!mIsReceiverRegistered) {
            if (mReceiver == null){
                IntentFilter intentF = new IntentFilter(MealConstants.BROADCAST_ACTION);
                mReceiver = new ClientBroadcastReceiver();
                LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mReceiver, intentF);
                mIsReceiverRegistered = true;
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //Removing our MealBroadcastReceiver
        if (mIsReceiverRegistered) {
            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mReceiver);
            mReceiver = null;
            mIsReceiverRegistered = false;
        }
    }


    private class ClientBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            final int actionPerformed = (int)intent.getExtras().get(MealConstants.MEAL_SERVICE_ACTION);

            if(actionPerformed == MealConstants.MEAL_SERVICE_ACTION_FIND_CLIENT_INFO){
                if(intent.getExtras().containsKey(MealConstants.KEY_PHONE_NOT_FOUND)){
                    //No data found
                    containerUserNotFound = (LinearLayout)inflatedView.findViewById(R.id.containerUserNotFound);
                    containerUserNotFound.setVisibility(View.VISIBLE);
                    userInforContainer = (LinearLayout)inflatedView.findViewById(R.id.containerUserInfo);
                    userInforContainer.setVisibility(View.GONE);

                } else {
                    containerUserNotFound = (LinearLayout)inflatedView.findViewById(R.id.containerUserNotFound);
                    containerUserNotFound.setVisibility(View.GONE);
                    userInforContainer = (LinearLayout)inflatedView.findViewById(R.id.containerUserInfo);
                    userInforContainer.setVisibility(View.VISIBLE);

                    TextView lastCheckin = (TextView)inflatedView.findViewById(R.id.lastCheckin);
                    TextView numberTotalOfCheckins = (TextView)inflatedView.findViewById(R.id.numberTotalOfCheckins);
                    TextView numberOfValidCheckins = (TextView)inflatedView.findViewById(R.id.numberOfValidCheckins);

                    Integer allCheckis = (Integer) intent.getExtras().get(MealConstants.KEY_COUNT_ALL_CHECKINS);
                    Integer validCheckins = (Integer) intent.getExtras().get(MealConstants.KEY_COUNT_VALID_CHECKINS);

                    numberTotalOfCheckins.setText(allCheckis.toString());
                    numberOfValidCheckins.setText(validCheckins.toString());
                    lastCheckin.setText((String) intent.getExtras().get(MealConstants.KEY_LAST_DATE_CHECKIN));

                }

            }

        }
    }

}
