package com.br.lfefox.lfefoxapp.fragment;

import android.app.Dialog;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.br.lfefox.lfefoxapp.R;
import com.br.lfefox.lfefoxapp.activity.AdminActivity;
import com.br.lfefox.lfefoxapp.activity.ResetPasswordActivity;
import com.br.lfefox.lfefoxapp.constants.MealConstants;
import com.br.lfefox.lfefoxapp.data.MealContract;
import com.br.lfefox.lfefoxapp.service.UserService;

/**
 * Fragment for Login
 */
public class LoginFragment extends Fragment {

    private View inflatedView;
    private boolean mIsReceiverRegistered = false;
    private LoginBroadcastReceiver mReceiver = null;

    public LoginFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        inflatedView = inflater.inflate(R.layout.fragment_login, container, false);

        bindBtnLogin();
        bindBtnReset();

        return inflatedView;
    }

    @Override
    public void onResume() {
        super.onResume();
        //Registering our MealBroadcastReceiver to receive call after MealService is done with its work
        if (!mIsReceiverRegistered) {
            if (mReceiver == null){
                IntentFilter intentF = new IntentFilter(MealConstants.BROADCAST_USER_ACTION);
                mReceiver = new LoginBroadcastReceiver();
                LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).registerReceiver(mReceiver, intentF);
                mIsReceiverRegistered = true;
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //Removing our MealBroadcastReceiver
        if (mIsReceiverRegistered) {
            LocalBroadcastManager.getInstance(getActivity().getApplicationContext()).unregisterReceiver(mReceiver);
            mReceiver = null;
            mIsReceiverRegistered = false;
        }
    }
    private void bindBtnLogin(){
        Button btn = (Button)inflatedView.findViewById(R.id.button_login_fragment);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean hasNoErrors = true;

                EditText username = (EditText)inflatedView.findViewById(R.id.loginUserName);
                if(username.getText().toString().equalsIgnoreCase("")
                        || username.getText().length() < 5){
                    username.setError(getResources().getString(R.string.label_mandatory));
                    hasNoErrors = false;
                }

                EditText password = (EditText)inflatedView.findViewById(R.id.loginPassword);
                if(password.getText().toString().equalsIgnoreCase("")
                        || password.getText().length() < 5){
                    password.setError(getResources().getString(R.string.label_mandatory));
                    hasNoErrors = false;
                }

                if(hasNoErrors){
                    //Calling UserService to check if there is an admin user registered
                    Intent intent = new Intent(getActivity().getApplicationContext(), UserService.class);
                    intent.setData(MealContract.UserEntry.CONTENT_URI);
                    intent.putExtra(MealConstants.USER_SERVICE_ACTION, MealConstants.USER_SERVICE_ACTION_LOGIN);

                    intent.putExtra(MealConstants.KEY_USER_NAME_EXTRA, username.getText().toString());
                    intent.putExtra(MealConstants.KEY_ACCESS_KEY_EXTRA, password.getText().toString());

                    getActivity().getApplicationContext().startService(intent);
                }

            }
        });

    }

    private void bindBtnReset(){
        Button btn = (Button)inflatedView.findViewById(R.id.button_reset_fragment);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intentAdmin = new Intent(getActivity().getApplicationContext(), ResetPasswordActivity.class);
                startActivity(intentAdmin);

                }
        });
    }

    private void displayErrorDialog(){

        Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.other_settings_confirmation);

        TextView textView = (TextView)dialog.findViewById(R.id.labelConfirmationMsg);
        textView.setText(R.string.msg_user_password_wrong);
        ImageView img = (ImageView) dialog.findViewById(R.id.imgConfirmationMsg);
        img.setImageResource(R.drawable.info);
        dialog.show();

    }
    /**
     * This class is responsible for treating UserService callbacks
     */
    private class LoginBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            //UserService can perform tasks for different fragments
            final int actionPerformed = (int)intent.getExtras().get(MealConstants.USER_SERVICE_ACTION);

            if(actionPerformed == MealConstants.USER_SERVICE_ACTION_LOGIN){
                int dataStatus = (int)intent.getExtras().get(MealConstants.EXTENDED_DATA_STATUS_USER);

                if(dataStatus == MealConstants.STATUS_LOGIN_OK){

                    Intent intentAdmin = new Intent(getActivity().getApplicationContext(), AdminActivity.class);
                    startActivity(intentAdmin);
                    getActivity().finish();

                } else if(dataStatus == MealConstants.STATUS_LOGIN_NOK){
                    EditText username = (EditText)inflatedView.findViewById(R.id.loginUserName);
                    EditText password = (EditText)inflatedView.findViewById(R.id.loginPassword);
                    username.setError(getResources().getString(R.string.label_invalid));
                    password.setError(getResources().getString(R.string.label_invalid));

                    displayErrorDialog();

                }

            }

        }
    }

}
