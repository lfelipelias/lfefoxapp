package com.br.lfefox.lfefoxapp.module;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.br.lfefox.lfefoxapp.MyApp;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;


@Module
public class StorageModule {
    final MyApp application;
    private Context context;


    public StorageModule(MyApp application){
        this.application = application;
        this.context = this.application.getApplicationContext();
    }

    @Singleton
    @Provides
    SharedPreferences provideSharedPreferences(){
        return PreferenceManager.getDefaultSharedPreferences(application);
    }

    @Singleton
    @Provides
    public Context provideApplicationContext() {
        return this.context;
    }

}
