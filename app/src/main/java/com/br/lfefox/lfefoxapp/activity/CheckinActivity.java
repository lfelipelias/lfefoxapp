package com.br.lfefox.lfefoxapp.activity;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.br.lfefox.lfefoxapp.R;
import com.br.lfefox.lfefoxapp.constants.MealConstants;
import com.br.lfefox.lfefoxapp.fragment.CheckinFragment;

public class CheckinActivity extends AppCompatActivity {

    private LinearLayout linearLayout;
    private FragmentManager fm;
    private boolean mIsReceiverRegistered = false;
    private MealBroadcastReceiver mReceiver = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkin);

        linearLayout = (LinearLayout)findViewById(R.id.linear_checkin_area);
        fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.linear_checkin_area, new CheckinFragment());
        ft.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();

        //Registering our MealBroadcastReceiver to receive call after MealService is done with its work
        if (!mIsReceiverRegistered) {
            if (mReceiver == null){
                IntentFilter intentF = new IntentFilter(MealConstants.BROADCAST_ACTION);
                mReceiver = new MealBroadcastReceiver();
                LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, intentF);
                mIsReceiverRegistered = true;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Removing our MealBroadcastReceiver
        if (mIsReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
            mReceiver = null;
            mIsReceiverRegistered = false;
        }
    }

    private class MealBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {

            int dataStatus = (int)intent.getExtras().get(MealConstants.EXTENDED_DATA_STATUS);
            String messageRuturn = (String)intent.getExtras().get(MealConstants.EXTENDED_DATA_RETURN_MESSAGE);

            EditText editText = (EditText)findViewById(R.id.phone_number_input);
            editText.setText(null);

            Intent intentConfirmation = new Intent(getApplicationContext(), ConfirmationActivity.class);
            intentConfirmation.putExtra(MealConstants.EXTENDED_DATA_RETURN_MESSAGE, messageRuturn);
            intentConfirmation.putExtra(MealConstants.EXTENDED_DATA_STATUS, dataStatus);
            startActivity(intentConfirmation);
        }
    }
}
