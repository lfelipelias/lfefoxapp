package com.br.lfefox.lfefoxapp.fragment;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.br.lfefox.lfefoxapp.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class WelcomeAdminFragment extends Fragment {


    public WelcomeAdminFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_welcome_admin, container, false);
    }

}
