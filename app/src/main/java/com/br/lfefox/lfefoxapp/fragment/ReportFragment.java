package com.br.lfefox.lfefoxapp.fragment;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.br.lfefox.lfefoxapp.R;
import com.br.lfefox.lfefoxapp.constants.MealConstants;
import com.br.lfefox.lfefoxapp.data.MealContract;
import com.br.lfefox.lfefoxapp.data.MealProviderUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class ReportFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private View inflatedView;
    private EditText phoneNumber;
    private EditText dtInit;
    private EditText dtEnd;
    private Button btnSearch;
    private ListView listOptions;
    private ReportSearchAdapter reportSearchAdapter;
    private final Calendar myCalendar = Calendar.getInstance();
    private int ID_LOADER = 0x04;

    private static final String DATE_PICKER_TAG = "dateFor";

    public ReportFragment() {}


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        inflatedView = inflater.inflate(R.layout.fragment_report, container, false);

        phoneNumber = (EditText) inflatedView.findViewById(R.id.reportPhoneNumber);
        phoneNumber.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MealConstants.MAX_SIZE_PHONE_NUMBER)});

        dtInit = (EditText) inflatedView.findViewById(R.id.reportFromDate);
        dtEnd = (EditText) inflatedView.findViewById(R.id.reportToDate);
        btnSearch = (Button) inflatedView.findViewById(R.id.reportBtnSearch);

        bindDatePicker(dtInit);
        bindDatePicker(dtEnd);
        bindBtnSearch(btnSearch);

        //Adapter is empty until the user makes the search
        reportSearchAdapter = new ReportSearchAdapter(getActivity().getApplicationContext(), R.layout.report_model_row, null);
        listOptions = (ListView) inflatedView.findViewById(R.id.listViewRowSearch);
        listOptions.setAdapter(reportSearchAdapter);



        return inflatedView;
    }


    private void bindBtnSearch(Button button){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isFieldsValid()) {
                    prepareSearch();
                }

            }
        });
    }

    /**
     * if user inform date from, he needs to inform date to as well
     *
     * @return boolean
     */
    private boolean isFieldsValid(){
        boolean isDataValid = true;

        String phone = phoneNumber.getText().toString().equalsIgnoreCase("")?null: phoneNumber.getText().toString();
        String beginDate = dtInit.getText().toString().equalsIgnoreCase("")?null: dtInit.getText().toString();
        String endDate = dtEnd.getText().toString().equalsIgnoreCase("")?null: dtEnd.getText().toString();

        if(phone != null && phone.length() < MealConstants.MIN_SIZE_PHONE_NUMBER){
            phoneNumber.setError(getResources().getString(R.string.label_phone_size_invalid, MealConstants.MIN_SIZE_PHONE_NUMBER));
            isDataValid = false;

        }

        if((beginDate != null && endDate == null)
                || (beginDate == null && endDate != null)){

            dtInit.setError(getResources().getString(R.string.label_both_dates_mandatory));
            isDataValid = false;
        }


        return isDataValid;
    }
    private void prepareSearch(){

        String phone = phoneNumber.getText().toString().equalsIgnoreCase("")?null: phoneNumber.getText().toString();

        Long beginLong = null;
        Long endLong = null;

        //Conversions for date
        String beginDate = dtInit.getText().toString().equalsIgnoreCase("")?null: dtInit.getText().toString();
        String endDate = dtEnd.getText().toString().equalsIgnoreCase("")?null: dtEnd.getText().toString();


        if(beginDate != null && endDate != null){
            SimpleDateFormat sdf = new SimpleDateFormat(getResources().getString(R.string.date_format_default), Locale.getDefault());


            try {


                Date dateStart = sdf.parse(beginDate);

                Calendar cal = Calendar.getInstance();
                cal.setTime(dateStart);
                //0:00 am
                dateStart = MealProviderUtils.getCalendarWithInitialHour(cal);
                beginLong = dateStart.getTime();

                Date dateEnd = sdf.parse(endDate);
                cal.setTime(dateEnd);
                //11:59pm
                dateEnd = MealProviderUtils.getCalendarWithFinalHour(cal);
                endLong = dateEnd.getTime();
            } catch (ParseException e) {
                Log.e("ReportFragment", "Error while parsing birthday date");
            }

        }
        Bundle bundle = new Bundle();

        if(phoneNumber != null){
            bundle.putString(MealConstants.KEY_PHONE_NUMBER, phone);
        }

        if(beginLong != null && endLong != null){
            bundle.putLong(MealConstants.KEY_DT_INIT, beginLong);
            bundle.putLong(MealConstants.KEY_DT_END, endLong);
        }

        getLoaderManager().restartLoader(ID_LOADER, bundle, this);


    }
    private void bindDatePicker(final EditText editText) {
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentTransaction ft = getFragmentManager().beginTransaction();

                DatePickerDialogFragment newFragment = new DatePickerDialogFragment();
                Calendar cal = Calendar.getInstance();
                newFragment.setInitialDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));

                newFragment.setListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        SimpleDateFormat sdf = new SimpleDateFormat(getResources().getString(R.string.date_format_default), Locale.getDefault());
                        editText.setText(sdf.format(myCalendar.getTime()));
                    }
                });

                newFragment.show(ft, DATE_PICKER_TAG + editText.getId());

            }
        });
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        String phoneParam = null;
        Long dtInitParam = null;
        Long dtEndParam = null;
        if(args.containsKey(MealConstants.KEY_PHONE_NUMBER)){
            phoneParam = (String)args.get(MealConstants.KEY_PHONE_NUMBER);
        }

        if(args.containsKey(MealConstants.KEY_DT_INIT) && args.containsKey(MealConstants.KEY_DT_END)){
            dtInitParam = (Long) args.get(MealConstants.KEY_DT_INIT);
            dtEndParam = (Long) args.get(MealConstants.KEY_DT_END);
        }

        String[] selectionCheckin = null;
        String where = null;
        if(phoneParam != null && dtInitParam != null){

            //User informed phone number and date range
            selectionCheckin = new String[]{String.valueOf(phoneParam), String.valueOf(dtInitParam), String.valueOf(dtEndParam)};
            where = MealProviderUtils.whereMealByPhoneAndDate;

        } else if(phoneParam != null){
            //User informed only phone number
            selectionCheckin = new String[]{String.valueOf(phoneParam)};
            where = MealProviderUtils.whereMealByPhone;

        } else if(dtInitParam != null){
            //User informed only date range
            selectionCheckin = new String[]{String.valueOf(dtInitParam), String.valueOf(dtEndParam)};
            where = MealProviderUtils.whereMealByDate;

        }


        CursorLoader loader = new CursorLoader(this.getActivity(),
                    MealContract.MealEntry.CONTENT_URI, null, where, selectionCheckin, null);
        return loader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        if(data.getCount() == 0){

            Dialog dialog = new Dialog(getActivity());
            dialog.setContentView(R.layout.other_settings_confirmation);

            TextView textView = (TextView)dialog.findViewById(R.id.labelConfirmationMsg);
            textView.setText(R.string.msg_no_result);
            ImageView img = (ImageView) dialog.findViewById(R.id.imgConfirmationMsg);
            img.setImageResource(R.drawable.info);
            dialog.show();

        }
        reportSearchAdapter.swapCursor(data);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        reportSearchAdapter.swapCursor(null);
    }


    private class ReportSearchAdapter extends CursorAdapter {

        private int layout;

        public ReportSearchAdapter(Context context, int layout,  Cursor cursor){
            super(context, cursor, false);
            this.layout = layout;

        }


        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            Cursor c = getCursor();
            final LayoutInflater inflater = LayoutInflater.from(context);
            View v = inflater.inflate(layout, parent, false);

            return v;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {

            TextView phoneNumber = (TextView)view.findViewById(R.id.reportPhoneRow);
            phoneNumber.setText(cursor.getString(1));

            Date dateCheckin = new Date(cursor.getLong(3));
            SimpleDateFormat sdf = new SimpleDateFormat(getResources().getString(R.string.date_format_default), Locale.getDefault());
            TextView dateText = (TextView)view.findViewById(R.id.reportDateRow);
            dateText.setText(sdf.format(dateCheckin));


            String status = cursor.getInt(2)==MealConstants.STATUS_CHECKIN_VALID ?
                    getResources().getString(R.string.active):getResources().getString(R.string.inactive);
            TextView statusText = (TextView)view.findViewById(R.id.reportStatusRow);
            statusText.setText(status);


        }
    }

}
