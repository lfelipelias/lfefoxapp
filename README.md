# README #


This app uses the following technologies:

- Dependency Injection with Dagger 2
- Services, Broadcast Receivers, Content Providers, CursorAdapters

MealRegister is an app that allows restaurant owners to keep track of their client's activities using their cellphone number. After a certain number of check-ins which can be configured in the admin area, the client receives a free meal.
The app has:

- Register area
- Check-in area: where users can do check-ins using their cellphone number
- Admin area: where admin can change settings and check reports.

The app supports English and Portuguese languages

Screenshots:


Welcome:
![welcomescreen.png](https://bitbucket.org/repo/rGb6j7/images/2111327361-welcomescreen.png)

Login:
![loginScreen.png](https://bitbucket.org/repo/rGb6j7/images/4262267817-loginScreen.png)

Checkin:
![checkinscreen.png](https://bitbucket.org/repo/rGb6j7/images/2549646470-checkinscreen.png)

Confirmation:
![checkinConfirmation.png](https://bitbucket.org/repo/rGb6j7/images/3342453210-checkinConfirmation.png)

Admin area:![welcomeAdmin.png](https://bitbucket.org/repo/rGb6j7/images/845518991-welcomeAdmin.png)
![drawerLayoutAdmin.png](https://bitbucket.org/repo/rGb6j7/images/1482095351-drawerLayoutAdmin.png)
![reportAdmin.png](https://bitbucket.org/repo/rGb6j7/images/499264612-reportAdmin.png)
![otherSettings.png](https://bitbucket.org/repo/rGb6j7/images/2804466036-otherSettings.png)

DatePickerDialog in Reset Password:
![resetPasswordDialogScreen.png](https://bitbucket.org/repo/rGb6j7/images/2569917024-resetPasswordDialogScreen.png)